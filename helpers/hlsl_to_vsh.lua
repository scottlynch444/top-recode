--[[
o-----------------------------------------------------------------------------o
| hlsl_to_vsh                                                                 |
(-----------------------------------------------------------------------------)
| By deguix                / An Utility for top-recode | Compatible w/ LuaJIT |
|                         ----------------------------------------------------|
|   Converts hlsl files to vsh files - compiles hlsl into vsh DX9/DX8 shaders.|
o-----------------------------------------------------------------------------o
--]]

function hlsl_to_vsh_dx8(shader_file)
	local file = io.open(shader_file, 'r+')
	local data = file:read("*a")
	data = data:gsub("c(%d-)%[", "c%[%1+")
	data = data:gsub("[^/][^/]dcl_(.-)\n", "  //dcl_%1\n")
	data = data:gsub("[^/][^T]vs_1_1", "  vs.1.1")
	file:seek("set")
	file:write(data)
	file:close()
	return true
end

function scandir_f(folder, func, linux)
    local i, t = 0, {}
	local pdir, pfile --ÌìÊ¹1.tga and ÌìÊ¹2.tga
	if linux then
		pdir = io.popen('ls -A "'..folder..'"')
	else
		pdir = io.popen('dir "'.. folder .. '" /b /ad')
	end
	for _directory in pdir:lines() do
		t[i] = scandir_f(folder .. "/" .. _directory, func, linux)
		i = i + 1
	end
    pdir:close()
	
	if linux then
		pfile = io.popen('ls "'..folder..'"')
	else
		pfile = io.popen('dir "'.. folder .. '" /b /a-d')
	end
    for filename in pfile:lines() do
        t[i] = filename
		func(folder .. "/" .. filename)
		i = i + 1
    end
    pfile:close()
    return t
end

function hlsl_to_vsh_folder(hlsl_folder, vsh_folder, dx8)
	dx8 = dx8 or false
	print('Converting folder\'s .hlsl files in "' ..hlsl_folder.. '" (inc subfolders)\r\nto .vsh in "'..vsh_folder..'".')
	local start_convert_time = os.clock()
	
	function timed_conversion(full_path_to_hlsl)
		local start_time = os.clock()
		--Modified from code found here:
		--https://stackoverflow.com/questions/5243179/what-is-the-neatest-way-to-split-out-a-path-name-into-its-components-in-lua/12191225
		local path, file_name, file_extension = string.match(full_path_to_hlsl, "(.-)([^\\/]-)%.?([^%.\\/]*)$")
		local full_path_to_vsh = vsh_folder.."/"..file_name..'.vsh'
		if (file_extension == 'hlsl') then
			local compilation_string = 'fxc /Tvs_2_a /Fc'..full_path_to_vsh..' '..full_path_to_hlsl
			if dx8 then
				compilation_string = 'fxc /Tvs_1_1 /O3 /Fc'..full_path_to_vsh..' '..full_path_to_hlsl
			end
			if not os.execute(compilation_string) then
				print("error when compiling with fxc")
			else
				if dx8 then
					hlsl_to_vsh_dx8(full_path_to_vsh)
				end
				print('('..(os.clock() - start_time)..' s)')
			end
		end
	end
	scandir_f(hlsl_folder, timed_conversion)
	print('Folder converted successfully in '..(os.clock() - start_convert_time)..'s: '..vsh_folder)
end