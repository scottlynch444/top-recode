--originally published by Momo
function LocalChat(char,player,text)
	local packet = packet:new()
	packet:add(DT_CMD, CMD_MC_TALK)
	packet:add(DT_4BN, GetCharID(char.role))
	packet:add(DT_STR, text)
	packet:send(player.role)
end

--posted by Insider:
-- If i'll say something in Argent then people in Thundoria will heard it. Pretty local. You should use GetChaByRange instead.
-- Also to show names you need to patch Game.exe (pretty simple, just one byte).

--[[
function Say(character,text)
local map_copy = GetChaMapCopy ( character )
local ply_num = GetMapCopyPlayerNum(map_copy)
local ps={}
local i = 1

BeginGetMapCopyPlayerCha ( map_copy )
for i = 1 ,ply_num , 1 do
ps[i]=GetMapCopyNextPlayerCha ( map_copy )
end

for i=1,ply_num,1 do
if(ps[i]~=0 and ps[i]~=nil)then
LocalChat( ps[i], character, text )
end
end
end
--]]