--TODO: Translation plan: First, use everything here, because it's easy, then move to .res file on release.
--TODO: Implement in-game triggers. AddTrigger function is key. AddNpcTrigger is for associating one with a npc. ClearTrigger and DeleteTrigger are never used for some reason... so game acumulates 100000 triggers?

--TODO: Implement bin files as lua files, then load everything. Have a function compile them into .txt then make server restart to load the updated txt's. Auto update client txt's too.

--[[
Remember whenever recompiling binaries:
- TOP 1.38 requires StringSet.bin, while newer versions don't. 2.0+ server uses a resource file in the system folder
instead with all the strings (which are not strings just for stringset).
- All versions require ResourceInfo.bin (don't forget it!).
- Add mall as last id in mapinfo.txt. If there's a new map, remember to add a new map in mapidnameslist and in server to adjust it.
- Keep same serverset.bin/.txt as provived.
- Server keeps the following files:

areaset.txt
characterinfo.txt
forgeitem.txt
int_cha_item.txt (not in client)
iteminfo.txt
lifelvup.txt (not in client)
saillvup.txt (not in client)
character_lvup.txt (not in client)
hairs.txt
skilleff.txt
skillinfo.txt
shipinfo.txt
shipiteminfo.txt

and probably client has some map files too...

TODO: python script to send these files to client on execution.

NOTE: Also, whenever grabbing bins from KOP/PKO, remember to grab its function modified version too.
Also means, delete bins in server, restart server, recompile bins in client, restart client.

--BUG ON GROUPSERVER: "resource" folder in "resource/script/Script01.lua" is hardcoded, so always message saying file doesn't exist.

TODO: Manu skills:
Create a database (like for gems) just for this.
Everytime a blueprint is opened, blueprint data is auto-stored in database.
But you keep the paper.
A npc will show which blueprints are stored by the name of the item
Clicking on the name of the item will show which items make that item.
Then a list of the amount of items needed for each blueprint is shown.
Clicking on one of those options will extract the blueprint for use/trade.
Need blueprint paper for extraction.
To store a blueprint, double-click on it (blueprint paper is left over).
Option to auto-store in database should be available so that there is a way to turn it off.

Analyze should have only 3 catalysts for the 3 slots used to craft an item.
Analyze should also work on manufactured items.

Skill level determines the level of the item that can be crafted/manufactured/analyzed/cooked.
Skill level goes up to max player level. That means, lvl 120 crafting = can make lvl 120 items.
Manufacturing/cooking should also be used to make skill books, any pots
including exp/drop exp pots, gems (instead of crafting), pet items (mostly cooking),
fireworks, stat resets (cooking), inv expansion.
Crafting should make rings, necks, arrows, corals, tools (which can be unique).

Quality of original items will make the quality of crafted items better (same in analyze).

Recipes will be remade. If an equipment is for a determined slot, simply a certain item is
needed (not a bunch of random ones). If an item is for a certain class, another item. Then
just 1 item need to be farmed differently for each item.

About manu, cooking: 1 item should just make it for pets, 1 just for gems, 1 just for regular manu,
1 just for fireworks and so on... then 1 for each subsection. If there's none, then just
use an item that is going to be sold for cheap at a npc.

Sea items will make only stuff that is specific to sea or ships (most of the time).

Cooking: will have the amount of cook in the dialog (my own modification - just like you were to see it cooking).
Only time is random, it requires skill to click at just the right time.
Manu: 100% chance of 1 item, random chance of extra (like normal).
Crafting: Making it 100% chance of success (will be very time consuming though).
Analyze: 100% chance because of just having 3 catalysts (should always result in an item).

TODO: This makes mob go follow owner (and a couple of extra commands):
SetChaHost(mob, ATKER)
SetChaAIType(mob, 13)
SetChaLifeTime(mob, 10000000)
--]]

--[[ TODO: Restructure items in iteminfo table.

Required item IDs and purpose:

x + 139 = Left glove weapon models

464 Transparent clothes (refers to body textures)
640 Transparent gloves (refers to body textures)
816 Transparent shoes (refers to body textures)
2000-2186 hair (most of them required by client)

Items IDs and names with special hard-coded effects:
193 Map switching (Quest reward icon - possible - item type 0)
194 G (gold) (Quest reward icon - item type 0)
195 Tax (Quest reward icon - item type 0)
1034 Star of Unity (Ring that shows EXP and type is not unique)
3849 Medal of valor (still to check - type 46 is unique).
2952 Special Operations Card (shows time - still to check - type 65 is unique).
128-134, 136-140 flying animation (hard-coded in .exe).

- Objectives:
-- Only unique models.
-- Model set for 1 char should go together as follows: Helmet > Armor > Gloves > Shoes.
-- Model set for many different chars should go together as follows: Lance > Phyllis > Carcise > Ami.
-- Full model set with weapons should go together as follows: Equips > Weapons.
-- All models go together as follows: Full model sets with weapons > Sorted equips following full model set rules globally.
-- Remember restriction on glove weapons.

-- All equips will be fused to themselves (then strengthening will be possible).

-- Other items will go to ID's 10000 and over.
-- Should reserve some ID ranges for special effects:
-- 10000-10100 for flying wings.

--]]

local ffi = require("ffi")

function _ALERT(...)
	print(...)
end

--Lua 5.2 foward compatibility: table.pack, table.unpack.
if not table.pack then
    function table.pack (...)
        return {n=select('#',...); ...}
    end
end

if not table.unpack then
    function table.unpack (...)
        return unpack (...)
    end
end

local start_loading_time = os.clock()

mod = 'default'

print(' ')
print('Loading "default" mod')
print('------------------------------------')

math.randomseed(os.time())

-- TO DO or comment section:

--[[
TODO: Create a npc/skill to tell which items a certain nearby player has. Also report its stats.
Can't detect bank items (except their amounts).
Everything else can be detected completely.

AddKbCap(role, 4) -- adds 4 backpack spaces.

TODO: Implement equip improvement gems (and also implement this under equip upgrade).

How are items going to be made:
no attack/def, but with the main stats.
add those through enhancements.

local ptnItem = GetEquipItemP( t[i] , 8)--ȡ������ָ��	pointer
local IdItem = GetItemID ( ptnItem )
local lvPerson = GetChaAttr(t[i] , ATTR_LV)
--Notice ( "���е�����" )
if IdItem==1034 and lvPerson < 41 then
local expItemNow = GetItemAttr( ptnItem , ITEMATTR_URE)*10   --100��ʼֵ
local expItemMax = GetItemAttr ( ptnItem , ITEMATTR_MAXURE)*10


--GetItemP(role, slot in inventory) -- this is useless.

--SetItemAttr ( Item , ITEMATTR_VAL_FUSIONID , Item_Lv ) --Sets equip appearance id.
--SetItemAttr ( Item , ITEMATTR_VAL_LEVEL , Item_Lv ) --Sets upgrade lvl.

--]]


print('Loading required section')

-- Required, otherwise server crashes (probably because max is set to 0, and equips add past that...)
for i = 0, 240, 1 do
	if (i >= 50) and (i <= 55) then
		SetChaAttrMax( i, (2^31)) -- Seems ATTR_B*** main stats cannot be negative, otherwise ATTR_AP it will spit out random numbers.
	else
		SetChaAttrMax( i, (2^32)-1) --signed, means there can be negative stats or levels.
	end
end

--dofile(GetResPath('script/'..mod..'/decompiler.lua'))

print('Loading pre-map handling files')
lfunc = dofile(GetResPath('script/'..mod..'/lua_functional.lua'))

kw = {
	add = function (x,y) return x + y end,
	sub = function (x,y) return x - y end,
	mul = function (x,y) return x * y end,
	div = function (x,y) return x / y end,
	mod = math.fmod,
	pow = function (x,y) return x ^ y end,
	eq = function (x,y) return x == y end,
	df = function (x,y) return x ~= y end,
	st = function (x,y) return x < y end,
	ste = function (x,y) return x <= y end,
	gt = function (x,y) return x > y end,
	gte = function (x,y) return x >= y end,
	_not = function (x) return not x end,
	_or = function (x,y) return x or y end,
	_and = function (x,y) return x and y end,
	con = function (x,y) return x .. y end,
	idx = function (x,y) return x[y] end,
	len = function (x) if type(x) == 'table' then return table.getn(x) end string.len(x) end,
	ass = function (x,y) x = y; return x end, --non-functional
	nilf = function () end
}

t_len = table.getn

--TODO: Make all constant definitions in BLARGH_1 = value and BLARGH_2 = value into table definitions
--with BLARGH.1 = value and BLARGH.2 = value. 

-- Load the tsv files first
local tsv_filelist = {
	'temp_settings.lua',
	'defs.lua',
	'util.lua',
	'tsv/charinfo.lua',
	'tsv/iteminfo.lua',
	'tsv/charaction.lua',
	'tsv/rest.lua'
}

-- Load all other files first
local filelist = {
	'birth_conf.lua',
	'item.lua',
	'char.lua',
	'player.lua',
	'char_stats.lua',
	'class.lua',
	'skill.lua',
	---
	--[[
	'kop/npcsdk.lua',
	'kop/missionsdk.lua',
	'kop/scriptsdk.lua',
	--kop scripts
	'kop/help.lua',

	'kop/ai_define.lua',
	'kop/ai_sdk.lua',
	'kop/ai.lua',

	'kop/calculate/item.lua',
	'kop/calculate/char.lua',
	'kop/calculate/guild.lua',
	--'kop/calculate/treasurehunt.lua', --uncomment if adding treasure hunt
	'kop/calculate/exp_and_level.lua',
	--'kop/calculate/JobType.lua',
	--'kop/calculate/AttrType.lua',
	--'kop/calculate/Init_Attr.lua',
	--'kop/calculate/ItemAttrType.lua',
	'kop/calculate/functions.lua',
	--'kop/calculate/AttrCalculate.lua',
	'kop/calculate/ItemEffect.lua',
	'kop/calculate/variable.lua',
	'kop/calculate/Look.lua',
	--'kop/calculate/forge.lua',
	'kop/calculate/ItemGetMission.lua',
	'kop/calculate/skilleffect.lua',

	'kop/missions/templatesdk.lua',
	'kop/npcs/NpcDefine.lua',
	--'kop/MisScript/ScriptDefine.lua',
	'kop/npcs/NpcScript08.lua',
	'kop/npcs/NpcScript07.lua',
	'kop/npcs/NpcScript06.lua',
	'kop/npcs/NpcScript05.lua',
	'kop/npcs/NpcScript04.lua',
	'kop/npcs/NpcScript03.lua',
	'kop/npcs/NpcScript02.lua',
	'kop/npcs/NpcScript01.lua',
	'kop/missions/EudemonScript.lua',
	'kop/missions/SendMission.lua',
	'kop/missions/MissionScript08.lua',
	'kop/missions/MissionScript07.lua',
	'kop/missions/MissionScript06.lua',
	'kop/missions/MissionScript05.lua',
	'kop/missions/MissionScript04.lua',
	'kop/missions/MissionScript03.lua',
	'kop/missions/MissionScript02.lua',
	'kop/missions/MissionScript01.lua',
	'kop/monsters/mlist.lua',
	--]]

	--- end
	'forge.lua',
	'packet.lua',
	'dialog.lua',
	'shop_trade.lua',
	'quests.lua',
	'npcs.lua',
	'misc.lua'
}

function r()
	start_time = os.clock()
	print('Loading main scripts')
	
	for i,filepath in ipairs(tsv_filelist) do
		print('- Loading file: '..tsv_filelist[i])
		dofile(GetResPath('script/'..mod..'/'..tsv_filelist[i]))
	end
	
	if tsv_created == nil then
		tsv_created = -1
	end

	if tsv_created ~= 1 then
		print('TSV text files are being recreated.')
		tsv.to_txt()
		if tsv_created == 0 then
			print('Anytime they need updating again, the tsv_created boolean in temp_settings.lua')
			print(' should be 0 or -1 (to make it do it always).')
			tsv_created = 1
			local temp_settings_file = io.open(GetResPath('script/'..mod..'/temp_settings.lua'), 'wb')
			temp_settings_file:write('tsv_created='..tostring(tsv_created))
			temp_settings_file:close()
			print('Exiting gameserver (TSV text files recreated).')
			os.exit()
		end
	else
		print('TSV text files are not going to be updated. To update,')
		print('set tsv_created boolean in temp_settings.lua to 0 or -1 (to make it do it always).')
	end
	
	for i,filepath in ipairs(filelist) do
		print('- Loading file: '..filelist[i])
		dofile(GetResPath('script/'..mod..'/'..filelist[i]))
	end
	
	print('Recompiled scripts in '..os.difftime(os.clock(), start_time).. 's.') -- Notice cannot be run in first execution (causes crash).
	--Notice('Recompiled scripts in '..os.difftime(os.clock(), start_time).. 's.') -- Notice cannot be run in first execution (causes crash).
end
r()

function check_item_valid ( role , Item )
	local Item_type = GetItemType ( Item )
	local Item_URE = GetItemAttr ( Item , ITEMATTR_URE )

	--If item durability is 0, then make it invalid to wear.
	if Item_type <= 29 or Item_type == 59 then
		if Item_URE ~= 0 and Item_URE <= 49 then
			return 0
		else
			return 1
		end
	end
	return 1
end

function join_map(role, map_name, map_copy)
	player = player_wrap(role)
	if player.id == -1 then
		Notice(player.name..' joined '..map_name..' ID: '..player_firstusableid())
		players[player_firstusableid()] = player
		--quests[1]:addtoplayer(player, 1) --for champion npc
	end
end

function leave_map(role, map_name, map_copy)
	player = player_wrap(role)
	Notice(player.name..' left '..map_name..' ID: '..player.id)
	players[player.id] = nil
end

function cha_timer(role, freq, time)
	if GetChaPlayer(role) then
		local now_tick = GetChaParam(role, 1)
		local resume_freq = 1
		SetChaParam(role, 1, now_tick + freq * time)
		--AttrRecheck(role) --making sure that all calcs are right without re-updating it all the time

		player = player_wrap(role)
		if player.id == -1 then
			player.id = player_firstusableid()
			players[player.id] = player
		end

		if math.fmod(now_tick, resume_freq) == 0 and now_tick > 0 then
			--AddChaSkill(player role, skill id, level, specific (1) or incremental (0), skill points used)			
			
			--Hp/Sp Recovery code

			--testing item
			--local item_pointer = GetChaItem2(player.role,2,1)
			--local item = item_wrap(item_pointer)

			--local light_item = light_item_wrap(1) --1 = Short Sword ID.

			--myprint(item.player)
			--print('bagamount:'..item.bagamount)
			--print('test')
			--myprint(light_item.report)
			--myprint(player.items_report)
			--print('test2')

			--Talk(2,"���ص�ռ��ʦ��","add","GetZhanBuInfo")

			--SetItemHost (role, role2) --sets the host of the items "role2" when "role1" dies.
			--SetItemFall ( count , item[1] , item[2] , item[3] , item[4] , item[5] , item[6], item[7] ,item[8],item[9],item[10] ) --Sets which drops does the the player drop, only in Check_SpawnResource and Check_Baoliao.

			--RemoveChaItem(player.role, 1, 1, 2, 0, 1, 0, 1) --sound is optional parameter
			--RemoveChaItem(player handle, item id, amount, bagtype (like getchaitem), bagslot, spacefreeinbag (if -1, it deletes it regardless whether it can be dropped or not), 1, warn if dropped with sound)


			--RemoveChaItem(player handle, ignored, amount, bagtype (like getchaitem), bagslot, operation, ignored, [warn if dropped with sound])

			--RemoveChaItem(player handle, ignored (id item), amount, bagtype (like getchaitem), bagslot, operation, ignored (not used by game either), [warn if dropped with sound])
			--bagtype: 0 = equipment
			--1 = also equipment - weirdly
			--2 = bag

			--purpose of function is to move item to bag, or drop it on floor, or destroy it.

			--spacefreeinbag change it to operation:
			--if 0, drops "amount" of it on the floor. If it comes from a locked source (like equipment), drops "amount" of items in slot 0 instead.
			--if 1, tries to move the item to backpack. If bag is full, then nothing is done.
			--if 2 (or any other number), deletes the item.

			--if the item refers to an item in equipment slot, then:
			--if spacefreeinbag > 0 and < amount, then moves the item to inventory bag. If already in inventory, it deletes it.
			--if spacefreeinbag > 0 and > amount, puts whatever fits in bag, and drops rest.
			--if spacefreeinbag = 0, then items are dropped directly on floor. If it's an equipment (or probably a somewhat locked source), it tries to first drop the items in slot 0 of the bag to free space for the equip (because it can't directly drop from equipment slots/locked sources).
			--if spacefreeinbag = -1, then it destroys the item.
			--if spacefreeinbag > 0 and there's no actual space on backpack, then item will not be moved.
			--same effect as spacefreeinbag = -1 is having the id same as the item refered to by bagslot, except that now those other options are available.

			--local item = item_wrap(GetChaItem(player.role,2,2))
			--item.stats[ITEMATTR_VAL_PARAM12] = 4294967296
			--print(item.stats[ITEMATTR_VAL_PARAM1])

			--local item = item_wrap(GetChaItem(player.role,2,0))
			-- RemoveChaItem(player.role, 0, 1, 2, 0, -1, 1, 1)
			--print(item.stats[ITEMATTR_URE])
			--            item.stats[ITEMATTR_ENERGY] = 240
			--            item.stats[ITEMATTR_MAXENERGY] = 240
			--			item.stats[ITEMATTR_URE] = 100
			--            item.stats[ITEMATTR_MAXURE] = 20100
			--print(item.stats[ITEMATTR_URE])

			--print(item.name, item.stats[ITEMATTR_VAL_DEF])
			--ResetItemFinalAttr(item.pointer)
			--AddItemFinalAttr(item.pointer, ITEMATTR_VAL_DEF, 10)
			--print(item.name, item.stats[ITEMATTR_VAL_DEF])

			--AddItemFinalAttr keeps on adding more and more stats to it...

			--ObligeAcceptMission(player.role, 1)

			--print(item.name)
			--AddItemEffect(player.role, item.pointer , 3502)

			--ChaPlayEffect(player.role,3742)
			
			--local main_hand_weapon = item_wrap(GetChaItem(player.role,1,9),player)
			
			--print(main_hand_weapon.name, main_hand_weapon.type)

			player.stats[ATTR_HP] = math.min(player.stats[ATTR_HP] + player.stats[ATTR_HREC], player.stats[ATTR_MXHP])
			player.stats[ATTR_SP] = math.min(player.stats[ATTR_SP] + player.stats[ATTR_SREC], player.stats[ATTR_MXSP])

			--myprint(GetItemID(GetChaItem(player.role, 2, 2)))

			----myprint(GetItemID(GetChaItem(player.role, 2, 2)))
		end
	end
end

print('Loading map folders')
--remember: This needs to be in the exact same order as in mapinfo
local mapidnameslist = {
	"garner",
	"magicsea",
	"darkblue",
	"lonetower",
	"eastgoaf",
	"secretgarden",
	"darkswamp",
	"abandonedcity",
	"abandonedcity2",
	"abandonedcity3",
	"puzzleworld",
	"puzzleworld2",
	"teampk",
	"jialebi",
	"garner2",
	"hell",
	"hell2",
	"hell3",
	"hell4",
	"hell5",
	"guildwar",
	"leiting2",
	"shalan2",
	"binglang2",
	"guildwar2",
	"yschurch",
	"07xmas",
	"sdBoss",
	"07xmas2",
	"prisonisland",
	"winterland",
	"mjing1",
	"mjing2",
	"mjing3",
	"mjing4",
	"mingyun",
	"starena13",
	"starena14",
	"starena15",
	"starena23",
	"starena24",
	"starena25",
	"starena33",
	"starena34",
	"starena35",
	--"kyjj_1",
	--"kyjj_2",
	--"kyjj_3",
	"DreamIsland",
	"starwalkway",
	"L4MAP"
}

local mapidnpcslist = {}

--TODO: Create "raw" folder when writing to prevent lua from erroring out right here.
npcs_init_file = io.open(GetResPath('script/'..mod..'/raw/npcs.lua'), 'wb')
for i = 1, table.getn(tsv.mapinfo), 1 do
	local map_name = tsv.mapinfo[i]['name']
	print('- Loading map folder: '..map_name)
	
	--Load npcs
	print(GetResPath(map_name .. '/' .. map_name .. 'npc.txt'))
	table.insert(mapidnpcslist, tsv_read(GetResPath(map_name .. '/' .. map_name .. 'npc.txt')))
	for j,line in pairsByKeys(mapidnpcslist[i]) do
		if mapidnameslist[i] ~= nil and mapidnpcslist[i][j][1] ~= nil and mapidnpcslist[i][j][11] ~= nil then
			npcs_init_file:write('function '..mapidnpcslist[i][j][11]..'() npc:init("'..mapidnameslist[i]..'",'..j..','..'"'..mapidnpcslist[i][j][1]..'") end\r\n')
		end
	end
	
	local ret = SetMap(map_name, i)
end
npcs_init_file:close()

print('------------------------------------')
print('Loading automaticly created files:')

local raw_filelist = {
	'raw/npcs.lua',
	'raw/skills.lua',
	'raw/skill_books.lua'
}

for i,filepath in ipairs(raw_filelist) do
	print('- Loading file: '..tsv_filelist[i])
	dofile(GetResPath('script/'..mod..'/'..raw_filelist[i]))
end

--function Skill_1_10(l)	return skill(0,1,10,l) end	--1000

--23,31 and 45

--0,1,2,3,4,5,10 have 1 arg (lvl)
--7 has 3 args (attacking_player, defending_player, lvl)
--6,8,9 have 2 args (player, lvl)
--[[
print('- Loading file: skill_raw.lua')
skill_init_file = io.open(GetResPath('script/'..mod..'/skills_raw.lua'), 'w')
local _skills = tsv_read(GetResPath('skillinfo.txt'))
for i,line in ipairs(_skills) do
	for j,cell in ipairs(line) do
		local k = -1
		if (j >= 22 and j <= 32) then
			k = j-22
		end
		if (j == 45) then
			k = 10
		end

		if ((k >= 0) and (string.sub(tostring(cell), 0, 5) == 'Skill')) then
			if ((k >= 0 and k <= 5) or (k == 10)) then
				skill_init_file:write('function '..cell..'(l) return skill(0,'..line[1]..','..k..',l) end\n')
			elseif (k == 7) then
				skill_init_file:write('function '..cell..'(a,d,l) return skill(0,'..line[1]..',7,l,a,d) end\n')
			elseif ((k == 6) or (k >= 8 and k <= 9)) then
				skill_init_file:write('function '..cell..'(a,l) return skill(0,'..line[1]..','..k..',l,a) end\n')
			end
		end
		-- if there are repetitive functions, it might be better to fix in the skillinfo itself.
	end
end

skill_init_file:write('\n')

local _skills_effects = tsv_read(GetResPath('skilleff.txt'))
for i,line in ipairs(_skills_effects) do
	for j,cell in ipairs(line) do
		local k = -1
		if (j >= 4 and j <= 6) then
			k = j-4
		end

		if ((k >= 0) and (string.sub(tostring(cell), 0, 5) == 'State')) then
			if (k == 0) then
				skill_init_file:write('function '..cell..'(l) return skill(1,'..line[1]..',0,l) end\n')
			elseif ((k >= 1) and (k <= 2)) then
				skill_init_file:write('function '..cell..'(a,l) return skill(1,'..line[1]..','..k..',l,a) end\n')
			end
		end
	end
end

skill_init_file:write('\n')

local _item_use = tsv_read(GetResPath('iteminfo.txt'))
for i,line in ipairs(_item_use) do
	for j,cell in ipairs(line) do
		if ((j == 87) and (tonumber(cell) == nil)) then --technically, all of the above should use this tonumber trick, but there are some awkward definitions in the bins...
			skill_init_file:write('function '..cell..'(p,i) return skill(2,'..line[1]..',0,0,p,i) end\n')
		end
	end
end

skill_init_file:close()
--]]

function bin_files_delta(client_folder, destructive)
	--Bin files delta - please look at the comments to see what changed
	
	--local _skill_info = tsv_read(GetResPath('skillinfo.txt'))
	--local _skill_eff = tsv_read(GetResPath('skilleff.txt'))
	--local _item_info = tsv_read(GetResPath('iteminfo.txt'))
	
	local skills_used = {}
	
	--local _character_info = tsv_read(GetResPath('characterinfo.txt'))
	
	--[[
	for i,v in pairsByKeys(_character_info) do
		for j,w in ipairs(string.split(',', v[44])) do
			if (skills_used[tonumber(w)] == nil) then
				skills_used[tonumber(w)] = {}
			end
			table.insert(skills_used[tonumber(w)], tostring(i)..','..v[1])
		end
	end
	--]]

	--tsv_write(GetResPath('character_skills.txt'), skills_used)
	
	--myprint(skills_used)
	
	--[[
	--Destructive section (don't do this - not yet anyways)
	if (destructive == true) then
		--Manufactured items need a rewrite. Only their current level 1 items will stay.
		--These are planned to be upgradable to lvl 7, by changing their item color.
		--Colors can also have other hidden attributes... but I won't use that for the manufacturing items.
		
		--Thus, manufacturing skill lvls 2-5 can be safely deleted. That leaves us with:
		--free skills range: 155-199, 392-443 (on top of manu lvl2-5). 342-377 are reserved for current extra top skills (after 502, 500-501 are free).
		old_min_i = 502
		old_max_i = 546
		new_min_i = 342
		for i=1,old_max_i-old_min_i+1 do
			--Todo: move 460, 461, 463 skills from exo to 462,463,464. I don't think other skills need to be moved, skills that were replaced are useless.
			--Todo: re-adjust ids for skill scripts in main code.
			_skill_info[342+i] = _skill_info[old_min_i+i]
			table.remove(_skill_info, old_min_i+i)
			-- I'm not going to remove Augury - number of skill effects can be hexxed, while skills themselves can't. So keep current skill effect ids.
		end
	end
	--]]
	
	--Non-destructive section
	
	--[[
	for i,v in pairsByKeys(_skill_info) do
		_skill_info[i][3] = '-1,' .. string.split(',', string.split(';', _skill_info[i][3])[1])[2] .. ';-2,-2;-2,-2;-2,-2' --Remove all class restrictions - 10 means max skill level.
		
		if (i == 241) then
			_skill_info[i][3] = '-1,3;-2,-2;-2,-2;-2,-2' --Remove all class restrictions.
		end
		
		_skill_info[i][7] = '-1,-1,0' --Remove all energy restrictions (for conches - durability is more useful).
		
		if (((i >= 453) and (i <= 459)) or (i == 475)) then
			_skill_info[i][4] = '-1,125;-2,-2;-2,-2;-2,-2'
		end
		
		if ((i == 215) or (i == 218) or (i == 219) or (i == 220)) then
			_skill_info[i][15] = '1'
		end
		
		_skill_info[i][14] = '1' --makes all skills land only, need item skills for sea.
		
		--Make short staff wielding cast lower spiritual bolt (dif is that it doesn't show char effect on ground).
		if (i == 36) then
			_skill_info[i][50] = '128' --instead of 92
			_skill_info[i][59] = '2003' --instead of 1015
			_skill_info[i][60] = '1200' --instead of 4000
			_skill_info[i][61] = '93' --instead of -1
			_skill_info[i][63] = '0' --instead of 131
		end
		
		if ((i >= 352) and (i <= 363)) then
			_skill_info[i][11] = '-1'
			--Necklace limitation to skills have to be applied scriptwise.
			--Dunno why Tigar Roar is only being applied to player himself.
		end
	end
	--]]
	
	--tsv_write(GetResPath('skillinfo.txt'), _skill_info)
	
	--Iteminfo
	--edit items that I want to be different here
	--Todo: Change item icons.
	--TODO: Make a way to change strings in language file.

	local f = io.open(GetResPath('script/'..mod..'/tsv/iteminfo_dump.lua'), 'wb')
	local a = tsv.iteminfo

	--[[
	for i, v in pairsByKeys(a) do
		local l = a[i]
--		if (item_type == '26') then --rings now can only go on right ring slot.
--			a[i][TII_slots] = {8}
--			l[TII_switchlocations] = {-1}
--		elseif (item_type == '29') then --corals now can only go on right weapon slot.
--			l[TII_slots] = {6}
--			l[TII_switchlocations] = {-1}
		elseif (item_type == '25') then --necklaces - define classes (418,419,420,421,461,462,463,495,497,739,4667,4672,4677),(5331-5340 - medal of honor),(6949-6958 - ultimate neck)
			l[TII_char_lvl] = '0'
			l[TII_STRP] = '0'
			l[TII_AGIP] = '0'
			l[TII_ACCP] = '0'
			l[TII_CONP] = '0'
			l[TII_SPRP] = '0'
			l[TII_LUKP] = '0'
			l[TII_ASPDP] = '0'
			l[TII_CRTRP] = '0'
			l[TII_ATKP] = '0'
			l[TII_MATKP] = '0'
			l[TII_DEFP] = '0'
			l[TII_MHPP] = '0'
			l[TII_MSPP] = '0'
			l[TII_FLEEP] = '0'
			l[TII_HITP] = '0'
			l[TII_CRTP] = '0'
			l[TII_DROPP] = '0'
			l[TII_HPRECP] = '0'
			l[TII_SPRECP] = '0'
			l[TII_MSPDP] = '0'
			l[TII_EXPP] = '0'
			l[TII_STR] = '0,0'
			l[TII_AGI] = '0,0'
			l[TII_ACC] = '0,0'
			l[TII_CON] = '0,0'
			l[TII_SPR] = '0,0'
			l[TII_LUK] = '0,0'
			l[TII_ASPD] = '0,0'
			l[TII_CRTR] = '0,0'
			l[TII_ATK] = '0,0'
			l[TII_MATK] = '0,0'
			l[TII_CON] = '0,0'
			l[TII_MHP] = '0,0'
			l[TII_MSP] = '0,0'
			l[TII_FLEE] = '0,0'
			l[TII_HIT] = '0,0'
			l[TII_CRT]= '0,0'
			l[TII_DROP] = '0,0'
			l[TII_HPREC] = '0,0'
			l[TII_SPREC] = '0,0'
			l[TII_MSPD] = '0,0'
			l[TII_EXP] = '0,0'
			l[TII_MDEF] = '0,0'
		end
		
		--2 handed weapons can now be dual-wielded but with a hefty price (like -50% move speed).
		if (l[TII_slots] == '9,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2') then
			l[TII_slots] = '9,6,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2'
			l[TII_switchlocations] = '-1,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2'
		end
	end
	--]]
	
	--for z,i in ipairs({1022,1024}) do a[i][TII_type] = '12' end --batteries becoming ammo types. (to be changed)
	--[[
	for z,i in ipairs({12,13}) do --ammo/arrows going on left right slot (to be changed)
		a[i][TII_maxamount] = '9999'
		a[i][TII_slots] = '7,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2'
		a[i][TII_switchlocations] = '-1,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2'
		a[i][TII_type] = '12'
	end
	--]]
	
	--[[
	for z,i in ipairs(range(860,864)) do
		a[i][TII_description] = "Strengthens the glow of this color in an equipment up to lvl 9. Stacks in 511's (1 stack = lvl 9 forge)."
		a[i][TII_maxamount] = '511'
	end
	local b
	b = a[860]
	b[TII_name] = 'Red Gem'
	b[TII_icon] = 'n1183'
	b[TII_dropmodel] = 'redstone01'
	b = a[861]
	b[TII_name] = 'Blue Gem'
	b[TII_icon] = 'n1185'
	b[TII_dropmodel] = 'bluestone01'
	b = a[862]
	b[TII_name] = 'Yellow Gem'
	b[TII_icon] = 'n1186'
	b[TII_dropmodel] = 'yellowstone01'
	b = a[863]
	b[TII_name] = 'Green Gem'
	b[TII_icon] = 'n1183s'
	b[TII_dropmodel] = 'greenstone01'

	b = a[885]
	b[TII_name] = 'Refining Gem'
	b[TII_icon] = 'n1181'
	b[TII_dropmodel] = 'refinestone'
	b[TII_maxamount] = '511'
	b[TII_description] = "Required for the forging of gems up to lvl 9. Stacks in 511's (1 stack = lvl 9 forge)."

	b = a[453]
	b[TII_name] = 'Fusion Scroll'
	b[TII_maxamount] = '99'
	b[TII_description] = "Required to change equipment's appearance and, on first use, to allow it to be upgradable."
	b = a[454]
	b[TII_name] = 'Fusion Catalyst'
	b[TII_maxamount] = '99'
	b[TII_description] = "Required to allow gems to adapt to the new equipment appearance."
	b = a[455]
	b[TII_name] = 'Strengthening Scroll'
	b[TII_maxamount] = '99'
	b[TII_description] = "Required to upgrade equips first fused."
	b = a[456]
	b[TII_name] = 'Strengthening Gem'
	b[TII_maxamount] = '511'
	b[TII_description] = "Required to upgrade equipment stats by 2% (per gem). Stacks in 511's because other gems need to."

	--Gem Combination cannot be transformed into "Mixing", thus the dialog is completely useless right now.
	--b = a[886]
	--b[TII_name] = 'Mixing Scroll'
	--b[TII_maxamount] = '99'
	--b[TII_description] = "Required to mix 2 different items to make 2 of a different item. For color gems, follow RGB color model."

	--Equipment Enhancement
	--Todo: Change item icons.
	b = a[88]
	b[TII_name] = 'Enhancement Pool'
	b[TII_maxamount] = '1'
	b[TII_description] = 'Contains enhancements to be applied to equipments. Double-click and select an equipment to apply this with the next available free enhancement pool number (maximum of 64 pool numbers).'
	b = a[89]
	b[TII_name] = 'Enhancement Pool Extractor' --TODO: make this a skill
	b[TII_maxamount] = '64'
	b[TII_description] = 'Double-click and select an equipment to extract its enhancement pool (also to free its enhancement pool number).'
	b = a[3302]
	b[TII_name] = 'Enhancement Pool Creator' --to be removed
	b[TII_maxamount] = '64'
	b[TII_description] = 'Double-click to create an enhancement pool with a certain pool number depending on the amount of enhancements in inventory (up to 64 enhancements to make it #64).'
	b = a[3303]
	b[TII_name] = 'Enhancement Pool Stat Checker' --TODO: make this a skill
	b[TII_maxamount] = '64'
	b[TII_description] = 'Double-click and select either an equipment with an enhancement pool applied, or and enhancement pool item to check its statistics.'
	--Todo: Change item icons.
	for z,i in ipairs(range(3304,3325)) do
		a[i][TII_maxamount] = '9999'
		a[i][TII_description] = 'Double-click and select equipment to which to apply the enhancement.'
	end
	a[3304][TII_name] = 'Strength Equipment Enhancement'
	a[3305][TII_name] = 'Accuracy Equipment Enhancement'
	a[3306][TII_name] = 'Agility Equipment Enhancement'
	a[3307][TII_name] = 'Constitution Equipment Enhancement'
	a[3308][TII_name] = 'Spirit Equipment Enhancement'
	a[3309][TII_name] = 'Luck Equipment Enhancement'
	a[3310][TII_name] = 'Maximum Hit Points Equipment Enhancement'
	a[3311][TII_name] = 'Maximum Spirit Points Equipment Enhancement'
	a[3312][TII_name] = 'Physical Attack Equipment Enhancement'
	a[3313][TII_name] = 'Physical Defense Equipment Enhancement'
	a[3314][TII_name] = 'Magical Attack Equipment Enhancement'
	a[3315][TII_name] = 'Magical Defense Equipment Enhancement'
	a[3316][TII_name] = 'Hit Rate Equipment Enhancement'
	a[3317][TII_name] = 'Dodge Rate Equipment Enhancement'
	a[3318][TII_name] = 'Critical Rate Equipment Enhancement'
	a[3319][TII_name] = 'Critical Resistance Equipment Enhancement'
	a[3320][TII_name] = 'Hit Points Recovery Equipment Enhancement'
	a[3321][TII_name] = 'Spirit Points Recovery Equipment Enhancement'
	a[3322][TII_name] = 'Attack Speed Equipment Enhancement'
	a[3323][TII_name] = 'Movement Speed Equipment Enhancement'
	a[3324][TII_name] = 'Experience Rate Equipment Enhancement'
	a[3325][TII_name] = 'Drop Rate Equipment Enhancement'
	--9 more items to use here

	b = a[418]
	b[TII_name] = 'Newbie Class Necklace'
	b[TII_DROPP] = '1000'
	b[TII_MSPDP] = '1000'
	b[TII_EXPP] = '1000'
	b = a[419]
	b[TII_name] = 'Warrior Class Necklace'
	b[TII_char_lvl] = '10'
	b[TII_STRP] = '1000'
	b[TII_ASPDP] = '50'
	b[TII_CRTRP]= '50'
	b[TII_ATKP] = '1000'
	b[TII_DEFP] = '100'
	b[TII_MHPP] = '100'
	b[TII_MSPP] = '100'
	b[TII_FLEEP] = '100'
	b[TII_HITP] = '100'
	b[TII_CRTP] = '50'
	b[TII_HPRECP] = '50'
	b[TII_SPRECP] = '50'
	b[TII_EXPP] = '100'
	b = a[420]
	b[TII_name] = 'Hunter Class Necklace'
	b[TII_char_lvl] = '10'
	b[TII_ACCP] = '1000'
	b[TII_ASPDP] = '50'
	b[TII_CRTRP] = '50'
	b[TII_ATKP] = '500'
	b[TII_DEFP] = '100'
	b[TII_MHPP] = '100'
	b[TII_MSPP] = '100'
	b[TII_FLEEP] = '100'
	b[TII_HITP] = '200'
	b[TII_CRTP] = '50'
	b[TII_HPRECP] = '50'
	b[TII_SPRECP] = '50'
	b[TII_EXPP] = '100'
	b = a[421]
	b[TII_name] = 'Thief Class Necklace'
	b[TII_char_lvl] = '10'
	b[TII_AGIP] = '1000'
	b[TII_ASPDP] = '100'
	b[TII_CRTRP] = '50'
	b[TII_ATKP] = '500'
	b[TII_DEFP] = '100'
	b[TII_MHPP] = '100'
	b[TII_MSPP] = '100'
	b[TII_FLEEP] = '150'
	b[TII_HITP] = '100'
	b[TII_CRTP] = '50'
	b[TII_HPRECP] = '50'
	b[TII_SPRECP] = '50'
	b[TII_EXPP] = '100'
	b = a[461]
	b[TII_name] = 'Knight Class Necklace'
	b[TII_char_lvl] = '10'
	b[TII_CONP] = '1000'
	b[TII_ASPDP] = '50'
	b[TII_CRTRP] = '100'
	b[TII_ATKP] = '500'
	b[TII_DEFP] = '150'
	b[TII_MHPP] = '150'
	b[TII_MSPP] = '100'
	b[TII_FLEEP] = '100'
	b[TII_HITP] = '100'
	b[TII_CRTP] = '0'
	b[TII_HPRECP] = '75'
	b[TII_SPRECP] = '50'
	b[TII_EXPP] = '100'
	b = a[462]
	b[TII_name] = 'Herbalist Class Necklace'
	b[TII_char_lvl] = '10'
	b[TII_SPRP] = '1000'
	b[TII_ASPDP] = '50'
	b[TII_CRTRP] = '50'
	b[TII_MATKP] = '1000'
	b[TII_DEFP] = '25'
	b[TII_MHPP] = '100'
	b[TII_MSPP] = '150'
	b[TII_FLEEP] = '100'
	b[TII_HITP] = '100'
	b[TII_CRTP] = '50'
	b[TII_HPRECP] = '50'
	b[TII_SPRECP] = '75'
	b[TII_EXPP] = '100'
	b = a[463]
	b[TII_name] = 'Explorer Class Necklace'
	b[TII_char_lvl] = '10'
	b[TII_LUKP] = '1000'
	b[TII_ASPDP] = '50'
	b[TII_CRTRP] = '50'
	b[TII_ATKP] = '350'
	b[TII_MATKP] = '350'
	b[TII_DEFP] = '100'
	b[TII_MHPP] = '100'
	b[TII_MSPP] = '150'
	b[TII_FLEEP] = '100'
	b[TII_HITP] = '100'
	b[TII_CRTP] = '100'
	b[TII_HPRECP] = '75'
	b[TII_SPRECP] = '75'
	b[TII_EXPP] = '100'
	b = a[495]
	b[TII_name] = 'Artisan Class Necklace'
	b[TII_char_lvl] = '10'
	b[TII_STRP] = '100'
	b[TII_AGIP] = '100'
	b[TII_ACCP] = '100'
	b[TII_CONP] = '100'
	b[TII_SPRP] = '100'
	b[TII_LUKP] = '100'
	b[TII_ASPDP] = '25'
	b[TII_CRTRP] = '25'
	b[TII_ATKP] = '100'
	b[TII_MATKP] = '100'
	b[TII_DEFP] = '150'
	b[TII_MHPP] = '150'
	b[TII_MSPP] = '150'
	b[TII_FLEEP] = '100'
	b[TII_HITP] = '100'
	b[TII_CRTP] = '50'
	b[TII_DROPP] = '1000'
	b[TII_HPRECP] = '50'
	b[TII_SPRECP] = '50'
	b = a[497]
	b[TII_name] = 'Merchant Class Necklace'
	b[TII_char_lvl] = '10'
	b[TII_STRP] = '100'
	b[TII_AGIP] = '100'
	b[TII_ACCP] = '100'
	b[TII_CONP] = '100'
	b[TII_SPRP] = '100'
	b[TII_LUKP] = '100'
	b[TII_ASPDP] = '25'
	b[TII_CRTRP] = '25'
	b[TII_ATKP] = '100'
	b[TII_MATKP] = '100'
	b[TII_DEFP] = '150'
	b[TII_MHPP] = '150'
	b[TII_MSPP] = '150'
	b[TII_FLEEP] = '100'
	b[TII_HITP] = '100'
	b[TII_CRTP] = '50'
	b[TII_HPRECP] = '50'
	b[TII_SPRECP] = '50'
	b[TII_MSPDP] = '500'
	b = a[739]
	b[TII_name] = 'Sailor Class Necklace'
	b[TII_char_lvl] = '10'
	b[TII_STRP] = '100'
	b[TII_AGIP] = '100'
	b[TII_ACCP] = '100'
	b[TII_CONP] = '100'
	b[TII_SPRP] = '100'
	b[TII_LUKP] = '100'
	b[TII_ASPDP] = '25'
	b[TII_CRTRP] = '25'
	b[TII_ATKP] = '100'
	b[TII_MATKP] = '100'
	b[TII_DEFP] = '150'
	b[TII_MHPP] = '150'
	b[TII_MSPP] = '150'
	b[TII_FLEEP] = '100'
	b[TII_HITP] = '100'
	b[TII_CRTP] = '50'
	b[TII_HPRECP] = '50'
	b[TII_SPRECP] = '50'
	b[TII_EXPP] = '1000'
	b = a[5331]
	b[TII_name] = 'Berserker Advanced Class Necklace'
	b[TII_char_lvl] = '40'
	b[TII_STRP] = '2000'
	b[TII_ASPDP] = '100'
	b[TII_CRTRP] = '100'
	b[TII_ATKP] = '2000'
	b[TII_DEFP] = '200'
	b[TII_MHPP] = '200'
	b[TII_MSPP] = '200'
	b[TII_FLEEP] = '200'
	b[TII_HITP] = '200'
	b[TII_CRTP] = '100'
	b[TII_HPRECP] = '100'
	b[TII_SPRECP] = '100'
	b = a[5332]
	b[TII_name] = 'Sharpshooter Advanced Class Necklace'
	b[TII_char_lvl] = '40'
	b[TII_ACCP] = '2000'
	b[TII_ASPDP] = '100'
	b[TII_CRTRP] = '100'
	b[TII_ATKP] = '1000'
	b[TII_DEFP] = '200'
	b[TII_MHPP] = '200'
	b[TII_MSPP] = '200'
	b[TII_FLEEP] = '200'
	b[TII_HITP] = '400'
	b[TII_CRTP] = '100'
	b[TII_HPRECP] = '100'
	b[TII_SPRECP] = '100'
	b = a[5333]
	b[TII_name] = 'Assassin Advanced Class Necklace'
	b[TII_char_lvl] = '40'
	b[TII_AGIP] = '2000'
	b[TII_ASPDP] = '200'
	b[TII_CRTRP] = '100'
	b[TII_ATKP] = '1000'
	b[TII_DEFP] = '200'
	b[TII_MHPP] = '200'
	b[TII_MSPP] = '200'
	b[TII_FLEEP] = '300'
	b[TII_HITP] = '200'
	b[TII_CRTP] = '100'
	b[TII_HPRECP] = '100'
	b[TII_SPRECP] = '100'
	b = a[5334]
	b[TII_name] = 'Champion Advanced Class Necklace'
	b[TII_char_lvl] = '40'
	b[TII_CONP] = '2000'
	b[TII_ASPDP] = '100'
	b[TII_CRTRP] = '200'
	b[TII_ATKP] = '1000'
	b[TII_DEFP] = '300'
	b[TII_MHPP] = '300'
	b[TII_MSPP] = '100'
	b[TII_FLEEP] = '100'
	b[TII_HITP] = '100'
	b[TII_CRTP] = '0'
	b[TII_HPRECP] = '150'
	b[TII_SPRECP] = '100'
	b = a[5335]
	b[TII_name] = 'Cleric Advanced Class Necklace'
	b[TII_char_lvl] = '40'
	b[TII_SPRP] = '2000'
	b[TII_ASPDP] = '100'
	b[TII_CRTRP] = '100'
	b[TII_MATKP] = '2000'
	b[TII_DEFP] = '50'
	b[TII_MHPP] = '200'
	b[TII_MSPP] = '300'
	b[TII_FLEEP] = '200'
	b[TII_HITP] = '200'
	b[TII_CRTP] = '100'
	b[TII_HPRECP] = '100'
	b[TII_SPRECP] = '150'
	b = a[5336]
	b[TII_name] = 'Voyager Advanced Class Necklace'
	b[TII_char_lvl] = '40'
	b[TII_LUKP] = '2000'
	b[TII_ASPDP] = '100'
	b[TII_CRTRP] = '100'
	b[TII_ATKP] = '700'
	b[TII_MATKP] = '700'
	b[TII_DEFP] = '200'
	b[TII_MHPP] = '200'
	b[TII_MSPP] = '300'
	b[TII_FLEEP] = '200'
	b[TII_HITP] = '200'
	b[TII_CRTP] = '200'
	b[TII_HPRECP] = '150'
	b[TII_SPRECP] = '150'
	b = a[5337]
	b[TII_name] = 'Blacksmith Advanced Class Necklace'
	b[TII_char_lvl] = '40'
	b[TII_STRP] = '200'
	b[TII_AGIP] = '200'
	b[TII_ACCP] = '200'
	b[TII_CONP] = '200'
	b[TII_SPRP] = '200'
	b[TII_LUKP] = '200'
	b[TII_ASPDP] = '50'
	b[TII_CRTRP] = '50'
	b[TII_ATKP] = '200'
	b[TII_MATKP] = '200'
	b[TII_DEFP] = '300'
	b[TII_MHPP] = '300'
	b[TII_MSPP] = '300'
	b[TII_FLEEP] = '200'
	b[TII_HITP] = '200'
	b[TII_CRTP] = '100'
	b[TII_DROPP] = '2000'
	b[TII_HPRECP] = '100'
	b[TII_SPRECP] = '100'
	b = a[5338]
	b[TII_name] = 'Tailor Advanced Class Necklace'
	b[TII_char_lvl] = '40'
	b[TII_STRP] = '200'
	b[TII_AGIP] = '200'
	b[TII_ACCP] = '200'
	b[TII_CONP] = '200'
	b[TII_SPRP] = '200'
	b[TII_LUKP] = '200'
	b[TII_ASPDP] = '50'
	b[TII_CRTRP] = '50'
	b[TII_ATKP] = '200'
	b[TII_MATKP] = '200'
	b[TII_DEFP] = '300'
	b[TII_MHPP] = '300'
	b[TII_MSPP] = '300'
	b[TII_FLEEP] = '200'
	b[TII_HITP] = '200'
	b[TII_CRTP] = '100'
	b[TII_HPRECP] = '100'
	b[TII_SPRECP] = '100'
	b[TII_MSPDP] = '1000'
	b = a[5339]
	b[TII_name] = 'Captain Advanced Class Necklace'
	b[TII_char_lvl] = '40'
	b[TII_STRP] = '200'
	b[TII_AGIP] = '200'
	b[TII_ACCP] = '200'
	b[TII_CONP] = '200'
	b[TII_SPRP] = '200'
	b[TII_LUKP] = '200'
	b[TII_ASPDP] = '50'
	b[TII_CRTRP] = '50'
	b[TII_ATKP] = '200'
	b[TII_MATKP] = '200'
	b[TII_DEFP] = '300'
	b[TII_MHPP] = '300'
	b[TII_MSPP] = '300'
	b[TII_FLEEP] = '200'
	b[TII_HITP] = '200'
	b[TII_CRTP] = '100'
	b[TII_HPRECP] = '100'
	b[TII_SPRECP] = '100'
	b[TII_EXPP] = '2000'
--	b = a[340]
--	b[TII_name] = '? Advanced Class Necklace'
--]]
	
	function merge_models_into_items(item_id_sets,item_name_set)
		--first, find the default set of items = always the one that comes first - just need to check first item of each set
		local default_set = -1
		local lowest_id_item = -1
		for i,set in ipairs(item_id_sets) do
			for j,item_id in ipairs(set) do
				if item_id ~= -1 and ((lowest_id_item == -1) or lowest_id_item > item_id) then
					lowest_id_item = item_id
					default_set = i
				end
			end
		end
		
		for i,def_item_id in ipairs(item_id_sets[default_set]) do
			for j,set in ipairs(item_id_sets) do
				if j ~= default_set then
					for k,item_id in ipairs(set) do
						if ((i == k) and (item_id ~= -1) and (a[item_id] ~= nil)) then
							for l,blargh in ipairs(range(0,5)) do
								if (a[item_id][TII_dropmodel+l] ~= '0') then
									a[def_item_id][TII_dropmodel+l] = a[item_id][TII_dropmodel+l]
								end
							end
						end
					end
				else
					a[def_item_id][TII_name] = item_name_set[i]
				end
			end
		end
	end
	
	merge_models_into_items({range(2820,2822),range(2817,2819),range(2820,2822),range(2829,2831)},{'Death\'s Robe', 'Death\'s Gloves','Death\'s Boots'})
	a[2835][TII_name] = 'Death\'s Hood'
	merge_models_into_items({range(6104,6107),range(6112,6115),range(6104,6107),range(6120,6123)},{'Aurora\'s Redemption','Aurora\'s Guard','Aurora\'s Blessing','Aurora\'s Mercy'})
	merge_models_into_items({range(6144,6146),range(6150,6152),range(6144,6146),range(6158,6160)},{'Ice-Covered Armor','Ice-Covered Gloves','Ice-Covered Boots','Ice-Covered Helmet'})
	merge_models_into_items({range(5661,5664),range(5665,5668),range(5669,5672),range(5673,5676)},{'Bloodmoon Helmet','Bloodmoon Armor','Bloodmoon Gloves','Bloodmoon Boots'})
	merge_models_into_items({range(5677,5680),range(5681,5684),range(5685,5688),range(5689,5692)},{'Wild West Hat','Wild West Shirt','Wild West Gloves','Wild West Boots'})
	merge_models_into_items({range(5945,5948),range(5949,5952),range(5953,5956),range(5957,5960)},{'Enchanted Forest Hat','Enchanted Forest Suit','Enchanted Forest Gloves','Enchanted Forest Boots'})
	merge_models_into_items({range(5970,5973),{-1,-1,-1,-1},range(5978,5981),{-1,-1,-1,-1}},{'White Demon\'s Hat','White Demon\'s Robe','White Demon\'s Gloves','White Demon\'s Boots'})
	merge_models_into_items({{-1,-1,-1,-1},range(5974,5977),{-1,-1,-1,-1},range(5982,5985)},{'Black Demon\'s Hat','Black Demon\'s Robe','Black Demon\'s Gloves','Black Demon\'s Boots'})
	merge_models_into_items({range(5726,5729),range(5730,5733),range(5734,5737),range(5738,5741)},{'Wonderer\'s Hat','Wonderer\'s Robe','Wonderer\'s Gloves','Wonderer\'s Boots'})
	merge_models_into_items({range(5742,5745),range(5746,5749),range(5850,5853),range(5854,5857)},{'Battle King\'s Hat','Battle King\'s Robe','Battle King\'s Gloves','Battle King\'s Boots'})
	merge_models_into_items({range(5651,5654),range(5655,5658),{5659,5660,5693,5694},range(5695,5698)},{'New Year\'s Hat','New Year\'s Robe','New Year\'s Gloves','New Year\'s Boots'})
	merge_models_into_items({{6267,6266,6268},{6264,6263,6265},{6267,6266,6268},{6273,6272,6274}},{'Mist Warrior Armor','Mist Warrior Gloves','Mist Warrior Boots'})
	merge_models_into_items({{6510,6509,6511},{6507,6506,6508},{6510,6509,6511},{6516,6515,6517}}, {'God Slayer Helm','God Slayer Gloves','God Slayer Boots'})
	merge_models_into_items({{6285,6284,6286},{6282,6281,6283},{6285,6284,6286},{6291,6290,6292}}, {'Water Break Armor','Water Break Gloves','Water Break Boots'})
	merge_models_into_items({range(5154,5156),{-1,-1,-1},range(5166,5168),{-1,-1,-1}}, {'Cavalry\'s Tiger Armor','Cavalry\'s Tiger Gauntlets','Cavalry\'s Tiger Greaves'})
	merge_models_into_items({range(5157,5159),{-1,-1,-1},range(5169,5171),{-1,-1,-1}}, {'Dragon\'s Capricious Robe','Dragon\'s Capricious Gloves','Dragon\'s Capricious Boots'})
	a[5160][TII_name],a[5161][TII_name],a[5162][TII_name] = 'Caesar\'s Ember Armor','Caesar\'s Ember Gauntlets','Caesar\'s Ember Greaves'

	merge_models_into_items({{5041,5045,5049},{5042,5046,5050},{5043,5047,5051},{5044,5048,5052}}, {'Catacomb\'s Armor','Catacomb\'s Gauntlets','Catacomb\'s Greaves','Catacomb\'s Crown'})
	merge_models_into_items({{5057,5061,5065},{5058,5062,5066},{5059,5063,5067},{5060,5064,5068}}, {'Buccaneer\'s Armor','Buccaneer\'s Gauntlets','Buccaneer\'s Greaves','Buccaneer\'s Crown'})
	merge_models_into_items({range(5205,5207),range(5195,5197),range(5201,5203),range(5198,5200)}, {'Royal Major Vest','Royal Major Gloves','Royal Major Boots'})
	merge_models_into_items({range(5264,5266),range(5267,5269),range(5270,5272),range(5273,5275)}, {'Christmas Shirt','Christmas Gloves','Christmas Boots'})
	merge_models_into_items({range(5287,5290),range(5291,5294),range(5295,5298),range(5299,5302)}, {'Fortune Cap','Fortune Robe','Fortune Gloves','Fortune Boots'})
	merge_models_into_items({range(5303,5305),range(5306,5308),range(5309,5311),range(5312,5314)}, {'Romantic Shirt/Blouse/Dress','Romantic Gloves/Muffs','Romantic Boots/Shoes'})
	merge_models_into_items({range(5315,5318),range(5319,5322),range(5323,5326),range(5327,5330)}, {'Piggy Beret/Bonnet/Cap','Piggy Shirt/Armor/Blouse/Costume','Piggy Gloves/Muffs','Piggy Boots/Shoes'})
	merge_models_into_items({range(5341,5343),range(5345,5347),range(5349,5351),range(5353,5355)}, {'Aries Diadem','Aries Armor/Forlorn/Costume','Aries Gauntlets/Muffs','Aries Greaves/Shoes'})
	merge_models_into_items({range(5356,5359),range(5360,5363),range(5364,5367),range(5368,5371)}, {'Taurus\'s Betreyal','Taurus\'s Loneliness','Taurus\'s Touch','Taurus\'s Flowing Sand'})
	merge_models_into_items({range(5372,5375),range(5376,5379),range(5380,5383),range(5384,5387)}, {'Gemini\'s Betreyal','Gemini\'s Loneliness','Gemini\'s Touch','Gemini\'s Flowing Sand'})
	merge_models_into_items({range(5404,5406),range(5407,5409),range(5410,5412),range(5413,5415)}, {'Baccalaureate Hat','Baccalaureate Gown','Baccalaureate Shoes'})
	merge_models_into_items({{-1,-1,-1},{-1,-1,-1},range(5416,5418),range(5419,5421)}, {'Student Gown','Student Gloves','Student Shoes'})
	merge_models_into_items({range(5422,5423),range(5424,5425),{-1,-1,-1},{-1,-1,-1}}, {'Romantic Pas Robe','Romantic Pas Shoes'})
	merge_models_into_items({range(5432,5433),range(5434,5435),{-1,-1,-1},{-1,-1,-1}}, {'Night Deity\'s Robe','Night Deity\'s Shoes'})
	merge_models_into_items({{5436},{5437},{-1},{-1}}, {'Romantic Pas Gloves (White)'})
	merge_models_into_items({{5440},{5441},{-1},{-1}}, {'Romantic Pas Gloves (Blue)'})
	merge_models_into_items({{5444},{5445},{-1},{-1}}, {'Romantic Pas Gloves (Black)'})
	merge_models_into_items({range(5456,5459),range(5460,5463),range(5464,5467),range(5468,5471)}, {'Big Crab\'s Rebellion','Big Crab\'s Loneliness','Big Crab\'s Touch','Big Crab\'s Quicksand'})
	merge_models_into_items({{5472},{5473},{5474},{5475}}, {'Yahoo Duckbilled Hat'})
	merge_models_into_items({{5476},{5477},{5478},{5479}}, {'Cacodaemon Cap'})
	merge_models_into_items({{5480},{5481},{5482},{5483}}, {'Centaur Helmet'})
	merge_models_into_items({{5484},{5485},{5486},{5487}}, {'Ice Dragon Helmet'})
	merge_models_into_items({range(5488,5490),range(5491,5493),range(5494,5496),range(5498,5500)}, {'Black Dragonkin Robe','Black Dragonkin Gloves','Black Dragonkin Shoes'})
	a[5497][TII_name] = 'Black Dragonkin Hat'
	merge_models_into_items({range(5501,5503),range(5504,5506),range(5507,5509),range(5511,5513)}, {'Kylinkin Robe','Kylinkin Gloves','Kylinkin Shoes'})
	a[5510][TII_name] = 'Kylinkin Hat'
	merge_models_into_items({range(5514,5516),range(5517,5519),{-1,-1,-1},{-1,-1,-1}}, {'Dragon\'s Armor','Dragon\'s Gauntlets','Dragon\'s Greaves'})
	merge_models_into_items({range(5521,5524),range(5525,5528),range(5529,5532),range(5533,5536)}, {'Leech Hat','Leech Robe','Leech Gloves','Leech Boots'})
	merge_models_into_items({range(5537,5540),range(5541,5544),range(5545,5548),range(5549,5552)}, {'Hacker Hat','Hacker Robe','Hacker Gloves','Hacker Boots'})
	merge_models_into_items({range(5553,5556),range(5557,5560),range(5561,5564),range(5565,5568)}, {'Azreal Hat','Azreal Robe','Azreal Gloves','Azreal Boots'})
	merge_models_into_items({range(5569,5572),range(5573,5576),{-1,-1,-1,-1},{-1,-1,-1,-1}}, {'Superman Hat','Superman Robe','Superman Gloves','Superman Boots'})
	merge_models_into_items({range(5577,5580),range(5581,5584),range(5585,5588),range(5589,5592)}, {'Fire Shadow Hat','Fire Shadow Robe','Fire Shadow Gloves','Fire Shadow Boots'})
	merge_models_into_items({range(5593,5596),range(5597,5600),range(5601,5604),range(5605,5608)}, {'Caribbean Hat','Caribbean Robe','Caribbean Gloves','Caribbean Boots'})	
	
	--Spinoff of character Hinata Hyuga from Naruto.
	a[5858][TII_name] = 'Ami Ninja Head'
	a[5859][TII_name] = 'Ami Ninja Body'
	a[5860][TII_name] = 'Ami Ninja Gloves'
	a[5861][TII_name] = 'Ami Ninja Boots'
	
	--Spinoff of the mixture of Smurfette and Sassette from The Smurfs.
	a[5862][TII_name] = 'Ami Blue Spirit Head'
	a[5863][TII_name] = 'Ami Blue Spirit Body'
	a[5864][TII_name] = 'Ami Blue Spirit Gloves'
	a[5865][TII_name] = 'Ami Blue Spirit Boots'

	a[5992][TII_name] = 'Templar\'s Robe'
	a[5993][TII_name] = 'Templar\'s Gloves'
	a[5994][TII_name] = 'Templar\'s Shoes'
	
	--[[
	a[61][TII_name] = 'Plaster Gloves' --2360 - 2366 too, but why should 2 of the same kind exist?
	a[62][TII_name] = 'Brawler Gloves'
	a[63][TII_name] = 'Master Gloves'
	a[64][TII_name] = 'Jagged Blades'
	a[65][TII_name] = 'Cosmo Blades'
	a[66][TII_name] = 'Tiger Gloves'
	a[67][TII_name] = 'Hands of Death'
	for z,i in ipairs(range(61,67)) do
		a[i][TII_type]= '6'
		a[i][TII_slots] = '9,-2,-2,-2,-2,-2,-2,-2,-2,-2'
		a[i][TII_switchlocations] = '9,3,-2,-2,-2,-2,-2,-2,-2,-2'
	end
	--]]
	
	--[[
	a[200][TII_name] = 'Plaster Gloves Gem Holder' --2360 - 2366 too, but why should 2 of the same kind exist?
	a[201][TII_name] = 'Brawler Gloves Gem Holder'
	a[202][TII_name] = 'Master Gloves Gem Holder'
	a[203][TII_name] = 'Jagged Blades Gem Holder'
	a[204][TII_name] = 'Cosmo Blades Gem Holder'
	a[205][TII_name] = 'Tiger Gloves Gem Holder'
	a[206][TII_name] = 'Hands of Death Gem Holder'
	for z,i in ipairs(range(200,206)) do
		a[i][TII_type]= '6'
		a[i][TII_slots] = '6,-2,-2,-2,-2,-2,-2,-2,-2,-2'
		a[i][TII_switchlocations] = '-1,-2,-2,-2,-2,-2,-2,-2,-2,-2'
	end
	--]]
	
	local new_item_info = {} --the change is so great, a new item needs to be created.
	
	new_item_info[-1] = a[-1]
	table.remove(new_item_info[-1], 1)
	for i,v in ipairs(tsv.iteminfo) do
		new_item_info[i] = {}
	end
	
	local valid_models_in_client = {}
	local missing_models_in_client = {}
	
	local char_model_folder = client_folder..'/model/character'
	local item_model_folder = client_folder..'/model/item'
	
	for i, v in ipairs(a) do
		local l = a[i]
		table.remove(l,1) --remove ID, to match iteminfo.lua
		local models = {l[TII_dropmodel-1],l[TII_lancemodel-1],l[TII_carsisemodel-1],l[TII_phyllismodel-1],l[TII_amimodel-1]} --added drop_model
		local add_to_item_info = true

		--if l[TII_slots] == '1,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2':
		--	l[TII_slots] = '0,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2'

		--why are ship item models in iteminfo?
		--[[
		if (l[TII_slots] == {12}) then
			add_to_item_info = false
		end
		--]]
		
		function model_exists(model) --needs a client
			if ((model ~= '0') and (file_exists(item_model_folder..'/'..model..'.lgo') or file_exists(char_model_folder..'/'..model..'.lgo'))) then
				return true
			else
				return false
			end
		end
		
		local equip_supported_classes = {}
		local uses_new_models = false
		
		for j,model in ipairs(models) do
			if ((model ~= '0') and (not model_exists(model))) then
				if (missing_models_in_client[model] == nil) then
					missing_models_in_client[model] = {}
				end
				table.insert(missing_models_in_client[model], {i, j})
				models[i] = '0'
			elseif ((model ~= '0') and model_exists(model)) then
				if (j ~= 1) then
					table.insert(equip_supported_classes, j-1)
				end
				if (valid_models_in_client[model] == nil) then
					valid_models_in_client[model] = {}
					uses_new_models = true
				end
				table.insert(valid_models_in_client[model], {i, j})
			end
		end
		
		if table.getn(equip_supported_classes) >= 1 then --has at least 1 of the classes model
			l = {l[TII_name-1], l[TII_icon-1], models[1], models[2], models[3], models[4], models[5],0,0,l[TII_type-1],0,0,0,0,true,true,true,true,true,1,1,50000,0,equip_supported_classes,'0','0',l[TII_slots-2],l[TII_switchlocations-2],0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},{0,100},0,{0,100},{15000,15000},3,0,0,0,0,0,0,0,0,0,{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0},{0,0},{0,0},l[TII_description-3]}
		else
			table.remove(l)
		end
		
		if (table.getn(equip_supported_classes) >= 1) and (uses_new_models == false) then
			add_to_item_info = false
		end

		if add_to_item_info then
			new_item_info[i] = l
		end
	end
	
	--tsv_write(GetResPath('iteminfo.txt'), new_item_info)
	--f:write(serpent.block(new_item_info))
	f:write(serialize_array(new_item_info,'\t',f))
	f:close()
	
	print('bin_files_delta requires a restart - doing it now')
	os.exit()
end

--client based functions
--not done on the client, because user can only edit lua files directly to be able to access these things, and also server txts are more accurate
--than client txts (if you decompile bins, you will know what I mean).

function get_missing_models(top_client_folder)
	local _item_info = tsv_read(GetResPath('iteminfo.txt'))

	--requires knowing the models available in client
	local char_model_folder = top_client_folder .. '/model/character/'
	local item_model_folder = top_client_folder .. '/model/item/'
	
	local missing_models_list = {}
	
	function model_exists(model)
		if ((model ~= '0') and ((os.rename(item_model_folder..model..'.lgo',item_model_folder..model..'.lgo')) or (os.rename(char_model_folder..model..'.lgo',char_model_folder..model..'.lgo')))) then
			return true
		end
		return false
	end
	
	for i, l in ipairs(_item_info) do
		for j=TII_dropmodel, TII_amimodel do
			if not (model_exists(l[j])) then
				if (j == TII_dropmodel) then
					table.insert(missing_models_list, item_model_folder+model+'.lgo')
				else
					table.insert(missing_models_list, char_model_folder+model+'.lgo')
				end
			end
		end
	end
	return missing_models_list
end

--when making changes to original binaries, uncomment the first of the following, run, comment it and repeat with next and so on:
--bin_files_delta(GetResPath('../client'),true)

--for edits to bin_files_delta, uncomment this, and comment the above
--bin_files_delta(GetResPath('../client'),true)

print('------------------------------------')
print('No errors. Loading finished in '.. os.difftime(os.clock(), start_loading_time)..' seconds.')

--myprint({'def',['abc']='abc',[5]='ghi',['jkl']='jkl'})


function Eudemon()
	print( "Eudemon init" )
	
	print(_VERSION)
end

-- text substitution with input value from selected region.

--[[
Ideas about the item models:
- make them apparels.
- create battery items which contain 7 stats, all chosen by user.
- use gems to give glow color to item (same as in top).
- fuse with apparel for use with a certain equip.
- apparel sets give bonuses (shown in system).
- halite transfers gems, catalyst makes fusion retain glow colors.
- unfuse function to get back catalyst and apparel.
- don't use itemaddstat. User won't be able to know the stat difference.

--]]

function MapInit_Callback()
--	dofile(GetResPath('script/'..mod..'/help/help.lua'))
--	dofile(GetResPath('script/'..mod..'/monster/mlist.lua'))
end
