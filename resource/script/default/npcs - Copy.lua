--AdjustTradeCess( character, p1, p2 ) -- adjusts tax rate.
--AddPetExp( character, p1, p2 ) -- adds pet exp.
--AddMoney( character, npc, p1 ) -- adds money.
--GiveItem( character, npc, p1, p2, p3 ) -- gives item.
--TODO: (low) Quests that skip own dialogs and that skip quest phases.

--TODO:
---New exp formula.
---Change exp given by mobs.
---Change "pewp" to a skill (no exp skill).
---Make a teleporter hub.
---Remove all quests.
---Remove all useless npc.
---Forging skills in the player, not another npc.

NPC_INIT = 0
NPC_UPDATE = 1

local npc={char=-1, id=-1, map='', name='', proc=nil, dialog=nil, trade=nil, quests={}}
local npcs_cur_id = 1
local npcs_max_id = 300
function npc.new(self, scriptid, o)
	o = o or {char=-1, id=-1, map='', name='', proc=nil, dialog=nil, trade=nil, quests={}}
	setmetatable(o, self)
	self.__index = self
	return o
end
function npc.init(self, map, id, name, o)
	for i, v in ipairs(npcs) do
		if v.name == name then
			self.id = id
			self.map = map
		end
	end
end

for i=1,npcs_max_id,1 do
	npcs[i] = npc:new(i)
end
function npc_id(name)
	for i,v in ipairs(npcs) do
		if v.name == name then
			return i
		end
	end
	return -1
end

-- npcs are 1 clickable with mouse to have action, and have icons in minimap.
-- Unfortunately, they are only ones with a message proc. That means, no way
-- to have choice without being a npc (that means, can still have dialog box
-- with just text as player).

--order of execution:
--ResetNpcInfo
--NpcFunction (actually, the function found in the npc list)
--GetNpcInfo

-- main npc initializer callback
-- called in order in first run, then if reloaded, it is called in reverse order.
-- called only after scripts are loaded.
function ResetNpcInfo(char_role, name) --called right after map files are called (System is a system npc).
	if name ~= 'System' and name ~= 'guardian' then --might be needed...
		SetNpcScriptID(char_role, npcs_cur_id)
		npcs[npcs_cur_id].char = char_wrap(char_role)
		npcs[npcs_cur_id].name = name
	end
	npcs_cur_id = npcs_cur_id+1
end

function ModifyNpcInfo(char, name, id) --called when asked to update npcs on client

	--[[ --dunno if this works, but if it works, then I should also put npc_init line here too...
	local scriptid = npc_id(name)
	npc_init(npcs[scriptid], scriptid)
	if table.getn(npc.quests) >= 1 then
	SetNpcHasMission( npc, 1 ) --makes the ! show on top of the npc (if player didn't finish them all)
	else
	SetNpcHasMission( npc, 0 )
	end
	--]]
	print('ModifyNpcInfo')
end

-- first npc initializer callback (only if npc specific function exists)
function GetNpcInfo(char, name)
	if name == 'System' then
		npc:init('',1,'System') --this part is never called?
	end
	local scriptid = npc_id(name)

	npc_init(npcs[scriptid], scriptid, nil, NPC_INIT)
	if table.getn(npcs[scriptid].quests) >= 1 then --I don't think anyone would want this differently...
		SetNpcHasMission(npcs[scriptid].char.role, 1)
	end
	--    else --at initialization, it's expected to be 0 by default.
	--        SetNpcHasMission(npcs[scriptid].char, 0)
	--    end
	----npc_init(npcs[npcs_cur_id], npcs_cur_id)
end

function NpcInfoReload( name,  func ) --apparently this is different on v2.
	print('NpcInfoReload')
	--[[
	local ret, char, scriptid = FindNpc( name )
	if ret == C_FALSE or char == nil or scriptid == nil then
	return
	end
	local npc = npc:new(char)
	npcs[table.getn(npcs)]=npc
	--]]
	--[[
	InitPage()
	InitTrade()
	InitNpcMission()
	NpcPointer = npc
	local str = "Initialization NPC ["..name.."] script notice successful!"
	PRINT( str )
	LG( "npcinit", str )
	--]]

	--myprint(func)
	func(npc)

	--[[
	if NpcMissionList.count > 0 then
	PRINT( "mission", "Set NPC bring quest label!" )
	SetNpcHasMission( npc, 1 )
	else
	PRINT( "mission", "set NPC does not carry quest label !" )
	SetNpcHasMission( npc, 0 )
	end
	--]]
end

--main network message callback

--berth means: port (in terms of boat). Port ID (berth ID) is specified when ports are created (check map files).
--Each port has a trading place. Boat has to be in the port for trading to work. Even though port could be in a place,
--the spawn could be somewhere else. Something could be done for this though:
--The ships are going to be called: Ship accounts.
--These ships accounts cannot be taken out of the place, but they can be transfered to other players.
--All items in inventory can be transfered to ship account.
--Impossible, can only "packbag" them, meaning, can only put items in... only way to take them out is with boat shops.
--Those items can be used, but cannot be equipped. Also cannot be hotkey'ed (so ship inv would have to be shown).

function NpcProc(player, char, _packet, scriptid) --scriptid comes from here
	--local npcname = GetCharName( npc )
	--local str = npcname..": Hi! You are looking for me? I am quite busy right now._......."
	--SendPage( character, npc, 0, str, nil, 0 )
	if player == nil or char == nil or _packet == nil then
		return
	end
	player = player_wrap(player)
	char = char_wrap(char)

	npc_init(npcs[scriptid], scriptid, player, NPC_UPDATE)

	local packet = packet:new(_packet)
	local cmd = packet:rem(DT_2BN)
	if cmd == CMD_CM_TALKPAGE then
		local event = packet:rem(DT_BN)
		if npcs[scriptid].proc ~= nil then
			npcs[scriptid].proc(npcs[scriptid], scriptid, player, cmd, event, nil)
		end
	elseif cmd == CMD_CM_FUNCITEM then
		local page = packet:rem(DT_BN)
		local item = packet:rem(DT_BN)+1
		if npcs[scriptid].proc ~= nil then
			npcs[scriptid].proc(npcs[scriptid], scriptid, player, cmd, page, item)
		end
	elseif cmd == CMD_CM_TRADEITEM then
		local tradetype = packet:rem(DT_BN)
		if tradetype == ROLE_TRADE_SALE then -- Selling an item
			if ((npcs[scriptid].trade.type ~= TRADE_BUY) and (npcs[scriptid].trade.type ~= TRADE_SALE)) then
				return
			end
			local index = packet:rem(DT_BN)
			local count = packet:rem(DT_BN)
			if count == 0 then
				return
			end
			--TODO: Customize sales with custom prices/item qualities (lower priority, because prices/qualities are not shown anyways)
			SafeSale(player.role, index, count) --asks if player wants to sell, and then sells it.
		elseif tradetype == ROLE_TRADE_BUY then -- Buying an item
			if ((npcs[scriptid].trade.type ~= TRADE_BUY) and (npcs[scriptid].trade.type ~= TRADE_SALE)) then
				return
			end
			local itemtype = packet:rem(DT_BN)+1
			local index1 = packet:rem(DT_BN)+1
			local index2 = packet:rem(DT_BN)
			local count = packet:rem(DT_BN)
			if count == 0 then
				return
			end
			--TODO: Customize buying with custom prices (lower priority, because prices are not shown anyways)
			SafeBuy(player.role, npcs[scriptid].trade.sections[itemtype][index1][1], index2, count) --asks if player wants to buy, and then buys it.
			-- no boats so far for the rest
		end
	elseif cmd == CMD_CM_BLACKMARKET_EXCHANGE_REQ then --���жһ�
		local timeNum = packet:rem(DT_2BN)
		local srcID = packet:rem(DT_2BN)
		local srcNum = packet:rem(DT_2BN)
		local tarID = packet:rem(DT_2BN)
		local tarNum = packet:rem(DT_2BN)
		local byIndex = packet:rem(DT_2BN)

		if KitbagLock(player.role,0) ~= LUA_TRUE then
			SystemNotice(player.role, "Shop Item Exchange: Inventory is locked." )
			return
		end

		if HasLeaveBagGrid(player.role,1) ~= LUA_TRUE then
			SystemNotice(player.role, "Shop Item Exchange: Inventory is full.")
			return
		end

		ExchangeReq(player.role, char.role, srcID, srcNum, tarID, tarNum, timeNum) --asks if player wants to exchange, then exchanges it
		--TODO: Make exchange actually take out items from npc's inventory too (by using something else than ExchangeReq).
		--Also, make it update the exchange too, while doing it, so to show how many items are left.
		--Certainly it's better to use dialogs for trading than these built-in dialogs...
	elseif cmd == CMD_CM_MISSION then --������
		local byCmd = packet:rem(DT_BN)
		if byCmd == MIS_SEL then
			local selindex = packet:rem(DT_BN)
			local ret, npc_quest_id, state = GetMissionInfo(player.role, char.id, selindex)

			if ret ~= LUA_TRUE then
				return SystemNotice(player.role, "MissionProc:obtain quest notice failed!")
			end
			if npcs[scriptid].quests[npc_quest_id] == nil then
				return SystemNotice(player.role, "MissionProc:Server does not have requested quest notice error!" )
			end

			player.interacting_with_npc = char
			local quest = quest:init(npcs[scriptid].quests[npc_quest_id][1].id, player, char)[1]
			--if quest.type == QUEST_TYPE_RANDOM then
			--    ret = HasRandMission( character, missionlist[id].id )
			--    local ret, miscount = GetRandMissionCount(player.role)
			--    print(ret, miscount,'getrandmissioncount')
			--    quest = quest:init(id, player, char)[miscount] -- GetCharMissionLevel(player.role) or GetCharGangLevel(player.role)
			--end

			if quest.type ~= QUEST_TYPE_RANDOM then
				if SetMissionTempInfo(player.role, char.id, npcs[scriptid].quests[npc_quest_id][1].id, state, quest.type) ~= LUA_TRUE then
					return SystemNotice(player.role, "MissionProc:set quest temporary data notice failed!")
				end
			end

			--HasLeaveBagGrid(player.role, mission.begin.baggrid) -- leave empty inventory spaces.
			--ret = RefreshMissionState( character, npc ) -- refreshes quest state
			--AddRandMissionNum( character, mission.id ) -- +1 on quest cycle number.
			--GetRandMissionNum( character, mission.id ) -- gets random quest number in cycle.
			--ResetRandMissionNum( character, mission.id ) -- resets random quests cycle.
			--HasRandMissionCount( character, mission.id, mission.loopinfo[loopnum].num ) -- has x amount of random quest cycles passed?

			--if quest.type == QUEST_TYPE_NORMAL then
			if state == QUEST_ACTION_DELIVERY then
				--if quest.type == QUEST_TYPE_RANDOM then
				--    AddRandMissionNum(player.role, quest.id)
				--    local ret, miscount = GetRandMissionCount(player.role, quest.id)
				--    if table.getn(quest:init(id, player, char)) > miscount then
				--        CompleteRandMissionCount(player.role, quest.id)
				--    end
				--end
				return quest:send(player, QUEST_EVENT_END)
			elseif state == QUEST_ACTION_ACCEPT then
				--[[
				if quest.type == QUEST_TYPE_RANDOM then
				print('testadasdspo1')
				local ret = HasRandMission(player.role, quest.id)
				print('testadasdspo2')
				if ret ~= C_TRUE then
				print('testadasdspo3')
				if IsMissionFull(player.role) == LUA_TRUE then
				return BickerNotice(player.role, "Quest slots are full. Couldn't accept quest." )
				end
				print('testadasdspo4')

				--local ret, id, state, tp = GetMissionTempInfo(player.role, char.id)
				--print('testadasdspo5')
				--if ret ~= LUA_TRUE then
				--    SystemNotice(player.role, "AcceptMission:obtain character dialogue temporary quest notice error!" )
				--    return C_FALSE
				--end
				--print('testadasdspo6')

				quest:execute(QUEST_STATE_START,QUEST_EXECUTE_ACTIONS)
				print('testadasdspo7')

				AddRandMission(player.role, id, id, quest.type, 1, 1, 1, 1, 1, 1)

				print('testadasdspo8')

				--for i, _quest in quest:init(id, player, char) do
				--    SetRandMissionData(player.role, id, i, 0, 0, 0, 0, 0, 0)
				--end
				print('testadasdspo9')

				print('before reset state')
				quests_reset_state(player)
				print('after reset state')

				print('testadasdspo10')

				quest:execute(QUEST_STATE_START,QUEST_EXECUTE_ACTIONS) --there are no conditions to start
				--local ret, miscount = GetRandMissionCount(player.role, quest.id) -- detects counting of series
				--local ret, miscount = GetRandMissionNum(player.role, quest.id) --detects number in the series
				print('testadasdspo11')
				AddRandMissionNum(player.role, quest.id)
				print('testadasdspo12')
				return quest:send(player, QUEST_EVENT_ONGOING)
				end
				end
				--]]
				return quest:send(player, QUEST_EVENT_START)
			elseif state == QUEST_ACTION_PENDING then
				return quest:send(player, QUEST_EVENT_ONGOING)
			else
				return SystemNotice(player.role, "SelMissionList:incorrect type of quest status notice!" )
			end
		elseif byCmd == MIS_BTNACCEPT then
			if IsMissionFull(player.role) == LUA_TRUE then
				return BickerNotice(player.role, "Quest slots are full. Couldn't accept quest." )
			end

			local ret, id, state, tp = GetMissionTempInfo(player.role, char.id)
			if ret ~= LUA_TRUE then
				SystemNotice(player.role, "AcceptMission:obtain character dialogue temporary quest notice error!" )
				return C_FALSE
			end

			local quest = quest:init(id, player, char)[1]
			quest:execute(QUEST_STATE_START,QUEST_EXECUTE_ACTIONS)

			AddMission(player.role, id, scriptid) --one of the actions usually used.

			quests_reset_state(player)

			return C_TRUE
		elseif byCmd == MIS_BTNDELIVERY then
			local byParam1 = packet:rem(DT_BN)
			local byParam2 = packet:rem(DT_BN)

			local ret, id, state, tp = GetMissionTempInfo(player.role, char.id)
			if ret ~= LUA_TRUE then
				SystemNotice(player.role, "CompleteMission:obtain character dialogue temporary quest notice error!" )
				return LUA_FALSE
			end

			local ret = HasMisssionFailure(player.role, id) --supposed to be id from table
			if ret == LUA_TRUE then
				BickerNotice(player.role, "Quest has failed, please select to abandon to clear quest log!" )
				return LUA_TRUE
			end

			local quest = quest:init(id, player, char)[1]
			quest:execute(QUEST_STATE_END,QUEST_EXECUTE_ACTIONS)

			ClearMission(player.role, id)
			quests_reset_state(player)
			--SetRecord(player.role, id) --leave it commented to test quests

			return C_TRUE
		end
	end

	return C_FALSE
end

--npc����״̬���?��
function NpcState(player_role, char_id, scriptid)
	print(char_id)
	for i = 1, 32, 1 do
		if npcs[scriptid].quests[i] == nil then
			break
		end
		local quest = quest:init(npcs[scriptid].quests[i][1].id,player_wrap(player_role), npcs[scriptid].char)[1]

		--if quest.type == QUEST_TYPE_NORMAL then
		--[[if quest.type == QUEST_TYPE_RANDOM then
		print('def_1')
		ret = HasRandMission(player.role, quest.id)
		print(ret, 'def_2')
		else
		]]
		local ret = HasMission(player.role, quest.id)
		--end
		if ret == C_TRUE then
			--[[if quest.type == QUEST_TYPE_RANDOM then
			print('def_3')
			local ret, miscount = GetRandMissionCount(player.role, quest.id) --GetRandMissionCount needs +1 on its values
			print(miscount, 'def_4')
			quest = quest:init(npcs[scriptid].quests[i][miscount].id,player_wrap(player_role), npcs[scriptid].char)[index]
			print('def_5')
			end
			--]]
			if (quest.show == ALWAYS_SHOW) or (quest.show == COMPLETE_SHOW) then
				if quest:execute(QUEST_STATE_END,QUEST_EXECUTE_CONDITIONALS) == C_TRUE then
					AddMissionState(player.role, char_id, i, QUEST_ACTION_DELIVERY)
				else
					AddMissionState(player.role, char_id, i, QUEST_ACTION_PENDING)
				end
			else
				--if (quest.show == ALWAYS_SHOW)or (quest.show == ACCEPT_SHOW) then
				AddMissionState(player.role, char_id, i, QUEST_ACTION_PENDING)
			end

			--end
			--local test, state = GetCharMission(player.role, char_id, i)
			--print(state) -- equals 0 when  always show (didnt start), equals 1 when already started.
			--elseif mission.show ~= COMPLETE_SHOW then
			--    AddMissionState(player.role, char_id, i, QUEST_EVENT_ONGOING)
		else
			if (quest.show == ALWAYS_SHOW) or (quest.show == ACCEPT_SHOW) then
				if quest:execute(QUEST_STATE_START,QUEST_EXECUTE_CONDITIONALS) == C_TRUE then
					AddMissionState(player.role, char_id, i, QUEST_ACTION_ACCEPT)
				end
			end
		end
		--end
	end

	return C_TRUE
end

function MissionLog(player_role, quest_sid)
	local player = player_wrap(player_role)
	if quest_sid == nil or quests[quest_sid] == nil then
		SystemNotice(player.role, 'Quest: ID "'..quest_sid..'" doesn\'t exist.')
		return
	end

	if quests[quest_sid][1].type == QUEST_TYPE_NORMAL or quests[quest_sid].type == QUEST_TYPE_GLOBAL then
		quests[quest_sid][1]:send(player, QUEST_EVENT_LOG)
		--[[
		elseif quests[quest_sid][1].type == QUEST_TYPE_RANDOM then
		quests[quest_sid][1]:send(player, QUEST_EVENT_LOG)
		--]]
	end

	quests_reset_state(player)
end

function CancelMission(player_role, quest_id, quest_sid)
	local player = player_wrap(player_role)
	local quest = quest:init(quest_id, player)[1]

	local boolean = quest:execute(QUEST_STATE_LOG, QUEST_EXECUTE_CONDITIONALS)
	if (boolean == C_TRUE) then
		quest:execute(QUEST_STATE_LOG, QUEST_EXECUTE_ACTIONS)
	end

	local ret = ClearMission(player_role, quest_id)
	if ret ~= C_TRUE then
		SystemNotice(player_role, 'Quest: Cleared "'.. quests[quest_id][1].name .. '" failed (probably doesn\'t exist already).')
	else
		SystemNotice(player_role, 'Quest: Clearing "'.. quests[quest_id][1].name .. '" successfully.')
	end

	quests_reset_state(player)

	return C_TRUE
end

--[[
--����װ��npc������Ϣ
function NpcInfoReload( name,  func )
PRINT( "NpcInfoReload: name, findnpc ", name, FindNpc )
local ret, npc, id = FindNpc( name )
if ret == LUA_FALSE or npc == nil or id == nil then
print( "unfound ["..name.."] NPC!" )
return
end
PRINT( "got npc notice, pointer =, id = ", npc, id )

print( GetCharName( npc ) )

ResetNpcInfo( npc, name )
PRINT( "ResetNpcInfo, npc = , name = ", npc, name )

func()
PRINT( "Func = ", func )

ModifyNpcInfo( npc, name, id )
PRINT( "ModifyNpcInfo, name = , id = ", name, id )
print( "Edit NPC ["..name.."] script notice successful!" )
end
--]]

function npc_stddialog_proc(npc, scriptid, player, cmd, p1, p2)
	if cmd == CMD_CM_TALKPAGE then
		if p1 == ROLE_FIRSTPAGE then
			player.interacting_with_npc = npc --to make sure quests are using right char for the player.
			npcs[scriptid].dialog:open(1,player)
		elseif p1 == ROLE_CLOSEPAGE then
			npcs[scriptid].dialog:close()
		end
	elseif cmd == CMD_CM_FUNCITEM then
		--remember to specify the target player on all kinds of network things here.
		npcs[scriptid].dialog.to_player = player
		npcs[scriptid].trade.to_player = player
		npcs[scriptid].dialog.pages[p1].options[p2][2](table.unpack(npcs[scriptid].dialog.pages[p1].options[p2][3]))
	end
end

function npc_tradeonly_proc(npc, scriptid, player, cmd, p1, p2)
	if cmd == CMD_CM_TALKPAGE then
		if p1 == ROLE_FIRSTPAGE then
			npcs[scriptid].trade:open(TRADE_SALE,player)
		elseif p1 == ROLE_CLOSEPAGE then
			npcs[scriptid].trade:close()
		end
	end
end

function npc_custominit_proc(npc, scriptid, player, cmd, p1, p2)
	if cmd == CMD_CM_TALKPAGE then
		if p1 == ROLE_FIRSTPAGE then
			npcs[scriptid].on_init(npc, scriptid, player, cmd, p1, p2)
			--elseif p1 == ROLE_CLOSEPAGE then  --not used
			--    npcs[scriptid].on_end(npc, scriptid, player, cmd, p1, p2)
		end
	end
end

function npc_garner_0() npc:init('garner',0,'Argent Teleporter - Jovial') end --GoToWhere1	
function npc_garner_1() npc:init('garner',1,'Argent Secretary - Salvier') end --r_talk01	
--r_trade01	
--r_talk86	
--r_talk08	
--r_talk02	
--r_talk06	
--r_talk10	
--r_talk03	
--r_talk13	
--r_talkXD	
--r_talk11	
--r_talkXD	
--r_talk14	
--r_talk04	
--r_talk15	
--r_talk16	
--r_talk17	
--r_talk07	
--BT_NpcSale001	
--r_talk18	
--BT_NewUser001	
--r_talk05	
--r_talk247	
--r_talk20	
--r_talk21	
--r_talk22	
--r_talk23	
--r_talk24	
--r_talk25	
--r_talk26	
--r_talk27	
--r_talkTH	
--r_talk29	
--r_talk30	
--r_talk31	
--r_talk32	
--r_talk33	
--r_talk34	
--r_talk35	
--r_talk72	
--r_talk37	
--r_talk38	
--GoToWhere2	
--r_talk39	
--r_talk40	
--r_talk41	
--r_talk42	
--r_talk43	
--r_talk44	
--r_talk46	
--r_talk87	
--r_talk88	
--transmittal	
--transmittal	
--r_talk92	
--r_talk93	
--r_talk94	
--r_talk96	
--r_talk97	
--r_talk98	
--r_talk101	
--r_talk102	
--r_talk103	
--r_talk104	
--r_talk105	
--r_talk107	
--r_talk108	
--special_1	
--r_talk151a	
--r_talk152a	
--special_lone	
--genesisa	
--genesisb	
--genesisc	
--genesisd	
--genesise	
--genesisf	
--0	
--0	
--piratec2	
--piratec1	
--0	
--Whale	
--r_talk150	
--r_talk151	
--r_talk152	
--r_talk157	
--r_talk158	
--r_talk129	
--r_talk146	
--r_talk159	
--transmittal	
--transmittal	
--transmittal	
--transmittal	
--r_talk160	
--r_talk147	
--r_talk122	
--r_talk162	
--r_talk163	
--r_talk164	
--r_talk165	
--r_talk178	
--r_talk179	
--r_talk180	
--r_talk181	
--r_talk182	
--r_talk183	
--r_talk184	
--max_talka2	
--r_talk255	
--r_talk253	
--r_talk207	
--r_talk210	
--r_talk211	
--r_talk212	
--r_talk213	
--r_talk214	
--r_talk215	
--r_talk216	
--island	
--island	
--island	
--island	
--piratec3	
--r_talk222	
--r_talk228	
--r_talk229	
--r_talk230	
--r_talk231	
--r_talk234	
--r_talk235	
--r_talk236	
--r_talk237	
--r_talk238	
--r_talk239	
--r_talk242	
--r_talk244	
--r_talk2000	
--r_talk244	
--card_01	
--Red_Skl01	
--b_talk25	
--b_talk25	
--b_talk26	
--woodmerch	
--max_talka3	
--Bosstele_2	
--max_talka2	
--kk_talk01	
--r_talk304	
--r_talk305	
--el_talk03	
--special_4	
--r_talk1000	
--r_talk1001	
--b_talk28	
--max_talka1	
--b_talk27	
--max_talka4	
--r_talk1002	
function npc_garner_164() npc:init('garner',164,'Fairy Merchant Tingle') end --r_merch01	
--r_talk1004	
--r_talk1005	
--r_talk1006	
--r_talk1007	
--r_talk1008	
--r_talk1009	
--r_talk1010	
--r_talk1011	
--r_talk1012	
function npc_garner_174() npc:init('garner',174,'Animal Nutritionist') end  --r_talk1013	
--r_talk1014	
--r_talk1015	
--r_talk1016	
--r_talk1017	
--r_talk1018	
--r_talk1019	
--r_talk1020	
--r_talk1021	
--r_talk1022	
--r_talk1023	
--r_talk1024	
--r_talk1025	
--r_talk1026	
--r_talk1027	
--r_talk1028	
--r_talk1029	
--r_talk1030	
--r_talk1031	
--r_talk1032	
--jackpot	
--rxcc	
--r_talk1034	
--oremerch	
--cchange_1	
--special_2	

--only way to link between npc procs and init is its name

function npc_init(npc, scriptid, player, state)

	if state == NPC_INIT then
		npcs[scriptid].proc = npc_tradeonly_proc
		npcs[scriptid].dialog = dialog:new(npc.char)
		npcs[scriptid].trade = shop_trade:new(npc.char)
	end
	
	--shorthand functions
	function n() --npc
		return npcs[scriptid]
	end
	
	function d() --npc dialog
		return npcs[scriptid].dialog
	end
	
	function p(page) --page table
		return npcs[scriptid].dialog.pages[page]
	end
	
	function t() --npc trade
		return npcs[scriptid].trade
	end
	
	function jtp(page) --jumptopage
		npcs[scriptid].dialog:open(page)
	end

			--some built-in top functions return many values...
			function HasItem_S(p,item_amounts)
				for i,v in ipairs(item_amounts) do
					local ret = HasItem(p.role,v[1],v[2])
					if ret == C_FALSE then
						return false
					end
				end
				return true
			end
			
			function HasMoney_S(p,gold)
				local result = HasMoney(p.role,gold,gold)
				if result == C_TRUE then
					return true
				end
				return false
			end
			
			function HasLeaveBagGrid_S(p,bagslots)
				local result = HasLeaveBagGrid(p.role,bagslots)
				if result == C_TRUE then
					return true
				end
				return false
			end
			
			function TakeItem_L(p,item_amounts)
				for i,v in ipairs(item_amounts) do
					local ret = TakeItem(p.role,v[1],v[2])
					if ret == C_FALSE then
						return false
					end
				end
				return true
			end
			
			function HasNoGuild_S(p)
				local result = NoGuild(p.role)
				if result == C_TRUE then
					return true
				end
				return false
			end
	
	if npc.name == 'Argent Teleporter - Jovial' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Jovial: Hi! I am the Teleporter! How may I help you?"
			p(1).options = {
				{"Go to Thundoria Castle",	GoTo, {player.role, 736, 1569, "garner"}},
				{"Andes Forest Haven",		GoTo, {player.role, 1007, 2966, "garner"}},
				{"Solace Haven",			GoTo, {player.role, 535, 2431, "garner"}},
				{"Maritime Laboratory",		GoTo, {player.role, 520, 2897, "garner"}},
				{"Xmas land (seasonal)",	MoveCity, {player.role, "xmas"}},
				{"Record Spawn point",		SetSpawnPos, {player.role, "Argent City"}}
			}
		end
	elseif npc.name == 'Argent Secretary - Salvier' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Salvier: Hi! You like my hair?? I think it looks Fantasthmic!"
			p(1).options = {
				{"Who are you?",							jtp, {2}},
				{"Where is this place?",					jtp, {3}},
				{"I think I have understand. Goodbye.",		jtp, {4}}
			}
			
			p(2).text = "Salvier: I am in charge of answering people's question for this city. If you meet up with any trouble, look for me."
			p(2).options = {
				{"Return",	jtp, {1}}
			}
			
			p(3).text = "Salvier: This is the most prosperous city in the whole of Ascaron. We have no lack of resources in any way. This is all thanks to our Duke who built this city."
			p(3).options = {
				{"Return",	jtp, {1}}
			}
			
			p(4).text = "Salvier: Ok sure, look for me again if you need more."
			p(4).options = {
				{"Ok, I want to know some more things.",	jtp, {5}}
			}
			
			p(5).text = "Salvier: Don't be shy. Ask away."
			p(5).options = {
				{"The Duke and founding of Argent",	jtp, {6}}
			}
			
			function givegandjtp(gold, page)
				AddMoney(player.role, gold, gold)
				jtp(page)
			end
			
			p(6).text = "Salvier: Many centuries ago, a war between the demons and Ascaron empire ensures. It lasted for years and taken a hugh toll on the citizen of the empire. Due to this, the Duke and his nephew leads the citizen to the current Argent City and build it from scratch. They endure 30 over years of hardship and manage to build this new city."
			p(6).options = {
				{"Wow! How sauve.",	givegandjtp, {100,7}}
			}
			
			p(7).text = "Salvier: As a great leader of a great city, I will treat all newcomers with hospitality. A gift of  100G! You can receive healing from the nurse if you are below Lv 5 too!"
			p(7).options = {
				{"Thank you!",	d().close, {d()}}
			}
			
			--Start( GetMultiTrigger(), 1 )
		end
	elseif npc.name == 'Blacksmith - Goldie' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "YARR! All handcrafted and forged with me own teeth!"
			p(1).options = {
				--{"Trade",					t().open,		{t(),TRADE_SALE}},
				{"Repair",					OpenRepair,		{player.role,npc.char.role}},
				{"Forge", 					OpenForge,		{player.role,npc.char.role}},
				{"Fusion",					OpenMilling,	{player.role,npc.char.role}},
				{"Special Set Upgrade",		OpenItemTiChun,	{player.role,npc.char.role}},
				{"Repair Lifeskill Tool",	OpenItemFix,	{player.role,npc.char.role}}
			}
			
			--t().sections = {{
			--		{0183},{0184}
			--	},{
			--	},{
			--	},{} }
		end
	elseif npc.name == 'Castle Guard - Peter' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Peter: Hi, I am the Guard, Peter. I keep the streets of this city free of thugs. I am also incharge of training new Swordsman. I also have taken the duty of selling swords and armor.  However they are of low status.  If you need to buy better equipment try the Tailor in Thundoria."
		
			p(1).options = {
				{"Swordsman Equipment and Weaponry",	t().open,		{t(),TRADE_SALE}}
			}
			
			t().sections = {{
					{0001},{0010},{0002},{0011},
					{0003},{0012},{0013},{0014},
					{0022},{0005},{0023},{1388},
					{1389},{1390},{1391},{1392},
					{1395},{1396},{1397},{1398},
					{1399},{0015},{0020},{0016},
					{0021},{0004},{1370},{1371},
					{1372},{1373},{1377},{1378},
					{1379},{1380},{1381},{1382},
					{1386},{1387}
				},{
					{0290},{0466},{0642},{0296},
					{0472},{0468},{0291},{0467},
					{0649},{0293},{0469},{0645},
					{0298},{0474},{0650},{0300},
					{0476},{0652},{0301},{0477},
					{0653},{0228},{0229},{0230},
					{0295},{0471},{0647},{0302},
					{0478},{0654},{0299},{0475},
					{0651},{0303},{0479},{0655}
				},{
				},{} }
		end
	elseif npc.name == 'Citizen - Margaret' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Margaret: You...Hahaha...Hello. Yesterday on television...Hahaha...Did you watch it. Hohoho...Its so funny! Hahaha..."
		
			p(1).options = {
				{"Bake Pastry",	jtp,	{2}}
			}
			
			p(2).text = "Margaret: You have good foresight! Not that I am boosting, but I watched the \"Iron Chef\" on television daily! The foodstuff I made are just delicious. Hehe"
			--	Text( 2, "Mix Elven Fruit Juice", MultiTrigger, GetMultiTrigger(), 1)
			--Text( 2, "Brew Red Date Tea", MultiTrigger, GetMultiTrigger(), 1)
			--Text( 2, "Make Mushroom Soup", MultiTrigger, GetMultiTrigger(), 1)
			--Text( 2, "Mix Stramonium Juice", MultiTrigger, GetMultiTrigger(), 1)
			--Text( 2, "Make Ice Cream", MultiTrigger, GetMultiTrigger(), 1)
		end
	elseif npc.name == 'Granny Beldi' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Old granny: How are you, young ducklings. You want a mystery Fruit? You need to go to DW and find the card letters: A, E, E, G, I, L, M, O, N, V and 100million Gold. Then bring them back here."
			p(1).options = {
				{"I have the cards",	jtp,	{2}, hidden=true}
			}
			
			if (HasItem_S(player,{
				{959,1},{963,2},{965,1},{967,1},{970,1},{971,1},{972,1},{973,1},{980,1}
			}) and HasMoney_S(player,100000000)) then
				p(2).options[2].hidden = false
			
			function takeitemsandgopage3()
				TakeItem_L(player, {
					{959,1},{963,2},{965,1},{967,1},{970,1},{971,1},{972,1},{973,1},{980,1}
				})
				jtp(3)
			end
			
			p(2).text = "Old Granny: WELL DONE! Now you what can those letters spell out?!"
			p(2).options = {
				{"Image Novel!",		takeitemsandgopage3,	{}},
				{"Enigma Love!",		takeitemsandgopage3,	{}},
				{"A Eel Moving!",		takeitemsandgopage3,	{}},
				{"Angel Movie!",		takeitemsandgopage3,	{}},
				{"A Evil Gnome!",		takeitemsandgopage3,	{}},
				{"Givee me quest NOW!",	takeitemsandgopage3,	{}}
			}
			
			p(3).text = "Okay! Now click on my new Green Quest XD"
		end
	elseif npc.name == 'Little Daniel' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			function excforancgenerator()
				if HasItem_S(player,{3933,10}) and HasMoney_S(player, 5000) then
					TakeItem_L(player,{3933,10})
					TakeMoney(player.role,5000)
					GiveItem(player.role,1812,1,4)
					jtp(1)
				end
				jtp(2)
			end
			
			p(1).text = "Little Daniel: Young adventurers should not be afraid to explore the world. There are many beautiful things waiting to be discovered. Have you been to the great shrine in the desert? How about the Lone Tower in the deep Sacred forest?"
			p(1).options = {
				{"Swordsman Equipment and Weaponry",			t().open,			{t(),TRADE_SALE}},
				{"Ancient Generator (5kG and 10 robot cores)",	excforancgenerator,	{}}
			}
			
			p(2).text = "Little Daniel: Sorry, you need to give me 10 Robot Cores and 5000G to make 1 Ancient Generator."
			p(2).options = {
				{"Return",			jtp,			{1}}
			}
			
			t().sections = {{
					{0073},{0080},{0074},{0081},
					{0075},{0082},{0076},{0083},
					{0077},{0084},{1415},{1416},
					{1417},{1418},{1419},{1422},
					{1423},{1424},{1425},{1426},
					{1443},{1444},{1445},{1446},
					{1447},{1450},{1451},{1452},
					{1460},{1461}
				},{
					{0335},{0511},{0687},{0336},
					{0512},{0688},{0338},{0514},
					{0690},{0337},{0513},{0689},
					{0340},{0516},{0692},{0339},
					{0515},{0691},{0341},{0517},
					{0693},{0342},{0518},{0694},
					{0345},{0521},{0697},{0343},
					{0519},{0695},{0380},{0556},
					{0732},{0351},{0527},{0703},
					{0386},{0562},{0738},{0352},
					{0528},{0704},{0350},{0526},
					{0702},{0354},{0530},{0706},
					{0353},{0529},{0705},{0356},
					{0532},{0708},{0255},{0531},
					{0707},{0357},{0533},{0709}
				},{
				},{} }
		end
	elseif npc.name == 'Physican - Ditto' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Ditto: Hi! The herbs I have are all gathered by meself. Have a look!"

			p(1).options = {
				{"Trade",	t().open,		{t(),TRADE_SALE}}
			}
		end
	elseif npc.name == 'Banker - Monica' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Monica: Hi, I am Banker Monica. Accounts can never go wrong with me around."

			p(1).options = {
				{"Vault",	OpenRepair,		{player.role,npc.char.role}}
			}
		end
	elseif npc.name == 'Superstar PK Admin' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Saint Arena Admin:\"I am proud that I did not leave anyone in the sky \""

			p(1).options = {
				{"Go to Argent City!",	GoTo, {player.role, 2231, 2788, "garner"}},
				{"Go to Superstar PK Snr!",	GoTo, {player.role, 77, 47, "hell2"}},
				{"Go to Superstar PK Jr!",		GoTo, {player.role, 74, 59, "hell4"}}
			}
		end
	elseif npc.name == 'General - William' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "William: Hi, I am General William. The highest authority around here."
		end
	elseif npc.name == 'Nurse - Gina' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Gina: Hello! I am Nurse Gina. Look for me if you are sick or injured!"
			p(1).options = {
				{"Full Heal",	function () player.stats[ATTR_HP] = player.stats[ATTR_MAXHP] end , {}}
			}
		end
	elseif npc.name == 'Shaitan Ambassador - Xiba' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
			p(1).text = "Xiba: Although I am an Ambassador, I need some time for myself too¡­Life cannot be just about work. You need to play to balance it too!"
		end
	elseif npc.name == 'Argent Chairman - Ronnie' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
			p(1).text = "Ronnie: I am the chairman who is in-charge of all commerce trading in Argent City. I can train you to become the richest Merchant in Ascaron." 
		end
	elseif npc.name == 'Oldman - Blurry' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
			p(1).text = "Blurry: The strawberry biscuit made by Beldi is so delicious!"
		end
	elseif npc.name == 'Grocery - Jimberry' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Jimberry: Hi, welcome! How can I help you?"
			
			p(1).options = {
				{"Trade",	t().open,		{t(),TRADE_SALE}}
			}
			
			t().sections = {{
				},{
				},{
					{3225},{3226},{3294},{3295},
					{3296},{2679},{2689},{2699},
					{2709},{1611},{1682},{1842},
					{1612},{1710},{1693},{4716},
					{1716},{1711},{3384},{3932},
					{1619},{2396},{1729},{4459},
					{1697},{1730},{1712},{1734},
					{1621},{1703},{2440},{2634},
					{2635},{2636},{2637},{2638},
					{2639}
				},{} }
		end
	elseif npc.name == 'Tailor - Granny Nila' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Granny Nila: Welcome! My Herbalist, SealMaster and Cleric weapons and armor are cheap and good! We also have trendy stuff, but if you are after serious equipment try Thundoria Tailor!"
			
			p(1).options = {
				{"Trade",	t().open,		{t(),TRADE_SALE}}
			}
			
			t().sections = {{
				},{
				},{
					{0097},{0104},{0098},{0105},
					{0099},{0106},{0101},{0107},
					{0102},{0108},{0100},{4301},
					{0103},{4302},{1427},{1428},
					{1429},{1430},{1431},{1432},
					{1433},{1434},{1435},{1436},
					{1437},{1438},{1440},{1441},
					{1462},{1463},{1464},{1465},
					{1466},{1467},{1468},{1469},
					{1470},{1471},{1472},{1473},
					{1475},{1476}
				},{
					{0365},{0541},{0717},{0372},
					{0548},{0724},{0366},{0542},
					{0718},{0373},{0549},{0725},
					{0368},{0544},{0720},{0374},
					{0550},{0726},{0367},{0543},
					{0719},{0375},{0551},{0727},
					{0369},{0545},{0721},{0376},
					{0552},{0728},{0370},{0546},
					{0722},{0378},{0554},{0730},
					{0371},{0547},{0723},{0379},
					{0555},{0731},{0383},{0559},
					{0735},{0359},{0535},{0711},
					{0381},{0557},{0733},{0360},
					{0536},{0712},{0389},{0565},
					{0741},{0361},{0537},{0713},
					{0390},{0566},{0742},{0362},
					{0538},{0714},{0385},{0561},
					{0737},{0363},{0539},{0715},
					{0392},{0568},{0744},{0388},
					{0564},{0740},{0382},{0558},
					{0734},{0391},{0567},{0743}
				} }
		end
	elseif npc.name == 'Assistant - Rouri' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Rouri: Hmm¡­I am wandering what secret does our chairman has? I am really interested to find out¡­"
		end
	elseif npc.name == 'Newbie Guide - Senna' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Senna: Hi, I am responsible for all newcomers and visitors. Things have changed a lot around here - pay attention or you'll miss out."
		
			p(1).options = {
				{"Take me somewhere I can level",	MoveCity,		{player.role,"levelme"}}
			}
		end
	elseif npc.name == 'Barmaid - Donna' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
			p(1).text = "Donna: Hi, I am Donna. Are you a sailor? Have you been to the legendary Galley Isle? Tell me the stories if you have."

		end
	elseif npc.name == 'Mystery Harbor Operator' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
			p(1).text = "Mist: Hi, I can help you salvage any sunken ships in this mysterious land. Do you need any help?"
		end
		--Todo: ships
--[[
	InitTrigger()
	TriggerCondition( 1, HasAllBoatInBerth, 12 )
	TriggerAction( 1, LuanchBerthList, 12, 2190, 2840, 0 )
	TriggerFailure( 1, JumpPage, 3 )
	Text( 1, "Set sail", MultiTrigger, GetMultiTrigger(), 1 )
	InitTrigger()
	TriggerCondition( 1, HasLuanchOut )
	TriggerAction( 1, RepairBoat )
	TriggerCondition( 2, HasBoatInBerth, 12 )
	TriggerAction( 2, RepairBerthList, 12 )
	TriggerFailure( 2, JumpPage, 4 )
	Text( 1, "Repair Ship", MultiTrigger, GetMultiTrigger(), 2 ) 
	InitTrigger()
	TriggerCondition( 1, HasLuanchOut )
	TriggerAction( 1, SupplyBoat )
	TriggerCondition( 2, HasBoatInBerth, 12 )
	TriggerAction( 2, SupplyBerthList, 12 )
	TriggerFailure( 2, JumpPage, 5 )
	Text( 1, "Refuel", MultiTrigger, GetMultiTrigger(), 2 ) 
	InitTrigger()
	TriggerCondition( 1, HasDeadBoatInBerth, 12 )
	TriggerAction( 1, SalvageBerthList, 12 )
	TriggerFailure( 1, JumpPage, 6 )
	Text( 1, "Salvage Ship", MultiTrigger, GetMultiTrigger(), 1 ) 
--]]
	elseif npc.name == 'Drunkyard - Anthony' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
			p(1).text = "Anthony: What? You want to find out something from me? Then you have found the right guy. However, only money talk. You have to pay for all information. Please input '/?keyword' into your text back for enquiry."

		end
	elseif npc.name == 'Youth - Tommy' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
			p(1).text = "Tommy: Hi, I am Tommy. What about you? Monsters are running rampant these days."
		end
	elseif npc.name == 'Manufacturer - Desmond' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
			p(1).text = "Desmond: Our artisan works are better than you humans, but you humans are just too arrogant to admit."
		end
	elseif npc.name == 'SMUGGLING BUNNY' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Smuggling Bunny: Banned items? You name it, I have it! What do you need?"
		end
	elseif npc.name == 'Navy Commander - Dessaro' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Dessaro: Hi, I am the highest in command in Thundoria. Do you wish to join us?"
			
			function create_guild()
				if 	HasMoney_S(player, 100000) and
					HasItem_S(player, {{1780,1}}) and 
					HasNoGuild_S(player) then
					CreateGuild(player.role)
				end
			end
			
			p(1).options = {
				{"Create a Navy Guild (100kG and 1 Stone of Oath)",	create_guild,{}}
			}
		end
	elseif npc.name == 'Bar Waitress - Mona' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Mona: Hi! Do you know the famous pirate, Sakura 13? She is my idol! I wish to leave this boring place and be a pirate like her! That's why I'm trading Refining gem vouchers for cash!"
			
			function moneyplusgemeqgem(gemitemid, amount, money, lvl)
				if HasItem_S(player, gemitemid,amount) and HasMoney_S(player,money) then
					TakeMoney(player.role, money, money)
					TakeItem(player.role, gemitemid, amount)
					GiveItem(player.role, 885, 1, 100+lvl)
					jtp(1)
				end
				jtp(2)
			end
			
			p(1).options = {
				{"Make Lv 3 Ref - 200k",	moneyplusgemeqgem,{3885,4,200000,3}},
				{"Make Lv 4 Ref - 200k",	moneyplusgemeqgem,{3885,8,400000,4}},
				{"Make Lv 5 Ref - 200k",	moneyplusgemeqgem,{3885,16,600000,5}},
				{"Make Lv 6 Ref - 200k",	moneyplusgemeqgem,{3885,32,800000,6}},
				{"Make Lv 7 Ref - 200k",	moneyplusgemeqgem,{3885,64,1000000,7}},
				{"Make Lv 8 Ref - 200k",	moneyplusgemeqgem,{3885,128,1200000,8}}
			}
			
			p(2).text = "Mona: Oi...you don't have enough ref vouchers or money!!"
		end
	elseif npc.name == 'Sailor - Dio' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Hi! I am Sailor Dio. Becareful when you are out in the open sea. There are many ferocious monsters out there, especially those sharks. They are demons¡­and they will bite off your leg! Just like mine! Oh no!...My leg! BTW: I also combine lv4-7 Gems from vouchers/chest of ascaron"
			
			--todo gem combiner.
		end
	elseif npc.name == 'Trader - Sanjay' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Hi! I am Sailor Dio. Becareful when you are out in the open sea. There are many ferocious monsters out there, especially those sharks. They are demons¡­and they will bite off your leg! Just like mine! Oh no!...My leg! BTW: I also combine lv4-7 Gems from vouchers/chest of ascaron"
			
			--todo gem combiner.
		end
	elseif npc.name == 'Fairy Merchant Tingle' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "My Name is Tingle. I love collecting all things Fairy. If you give me fairy items, I will give you special gifts, Kaloooompa!!"
			p(1).options = {
				{"Books for Fairies",	jtp, {2}},
				{"Energy",				jtp, {3}},
				{"Metal",				jtp, {4}},
				{"Skill Books 1-5",		jtp, {5}},
				{"Skill Books 6-10",	jtp, {6}}
			}

			function mt(take_table, give_table, page)
			--Trigger: Give item to player, then go back to a page.
				if HasItem(player.role, take_table[1], take_table[2]) == C_TRUE then
					TakeItem(player.role, 0, take_table[1], take_table[2])
					GiveItem(player.role, 0, give_table[1], give_table[2], 4) --quality set to 4 (in terms of non-equips, it makes no difference).
					d():open(page,player)
				else
					d():open(17,player)
				end
			end

			p(2).text = "Did you know pets love to read? This is my collection of Favourite Fairy Books. Easy Books are worth 50 Fairy Coins, Medium ones are 50 Elven Signets."
			p(2).options = {
				{"Novice Pet Manufacturing", 	mt, {{855,	50},{1055, 1},2}},
				{"Standard Pet Manufacturing", 	mt, {{2588,	50},{1056, 1},2}},
				{"Novice Pet Crafting", 		mt, {{855,	50},{1058, 1},2}},
				{"Standard Pet Crafting", 		mt, {{2588,	50},{1059, 1},2}},
				{"Novice Pet Analyze", 			mt, {{855,	50},{1061, 1},2}},
				{"Standard Pet Analyze", 		mt, {{2588,	50},{1062, 1},2}},
				{"Novice Pet Cooking", 			mt, {{855,	50},{1064, 1},2}},
				{"Standard Pet Cooking", 		mt, {{2588,	50},{1065, 1},2}}
			}

			p(3).text = "I have long tracked the source of fairy magic. So far my discoveries have lead me to these chunks of Energy. I just don't know what they do."
			p(3).options = {
				{"Fairy Coin x30 - Compressed Energy I", 			mt, {{855,	30},{2617, 1},3}},
				{"Fairy Coin x50 - Compressed Energy II", 			mt, {{855,	50},{2619, 1},3}},
				{"Elven Signet x10 - Compressed Energy III", 		mt, {{2588,	10},{2622, 1},3}},
				{"Elven Royal Signet x1 - Compressed Energy IV", 	mt, {{2589,	1},	{2624, 1},3}}
			}
			
			p(4).text = "I have long tracked the source of fairy magic. So far my discoveries have lead me to these chunks of Rare Metals. I just don't know what they do."
			p(4).options = {
				{"Fairy Coin x30 - Solid Metal", 				mt, {{855,	30},{2641, 1},4}},
				{"Fairy Coin x50 -  Pressurised Metal", 		mt, {{855,	50},{2640, 1},4}},
				{"Elven Signet x10 -  Support Metal", 			mt, {{2588,	10},{2642, 1},4}},
				{"Elven Signet x10 -  Tenacious Metal", 		mt, {{2588,	10},{2643, 1},4}},
				{"Elven Signet x25 -  Waterproof Metal", 		mt, {{2588,	25},{2644, 1},4}},
				{"Elven Signet x25 -  Insulating Metal", 		mt, {{2588,	25},{2649, 1},4}}
			}
			
			p(5).text = "I have also written books on how to enjoy activities with your fairies. Each volume has 4 different activities."
			p(5).options = {
				{"Fairy Coin x2 - Volume 1", 			jtp, {7}},
				{"Fairy Coin x10 - Volume 2", 			jtp, {8}},
				{"Fairy Coin x50 - Volume 3", 			jtp, {9}},
				{"Fairy Coin x150 - Volume 4", 			jtp, {10}},
				{"Elven Signet x5 - Volume 5", 			jtp, {11}}
			}
			
			p(6).text = "I have also written books on how to enjoy activities with your fairies. Each volume has 4 different activities."
			p(6).options = {
				{"Elven Signet x25 - Volume 6", 		jtp, {12}},
				{"Elven Signet x50 - Volume 7", 		jtp, {13}},
				{"Elven Royal Signet x2 - Volume 8", 	jtp, {14}},
				{"Elven Royal Signet x5 - Volume 9", 	jtp, {15}},
				{"Elven Royal Signet x10 - Volume 10", 	jtp, {16}}
			}
			
			p(7).text = "I have also written books on how to enjoy activities with your fairies. I can give you volume 1 for 2 fairy coins."
			p(7).options = {
				{"Level 1 Manufacturing Guide", mt, {{855,2},{2679,1},7}},
				{"Level 1 Cooking Guide", 		mt, {{855,2},{2689,1},7}},
				{"Level 1 Crafting Guide", 		mt, {{855,2},{2699,1},7}},
				{"Level 1 Analyse Guide", 		mt, {{855,2},{2709,1},7}}
			}
			
			p(8).text = "I have also written books on how to enjoy activities with your fairies. I can give you volume 2 for 10 fairy coins."
			p(8).options = {
				{"Level 2 Manufacturing Guide", mt, {{855,10},{2680,1},8}},
				{"Level 2 Cooking Guide", 		mt, {{855,10},{2690,1},8}},
				{"Level 2 Crafting Guide", 		mt, {{855,10},{2700,1},8}},
				{"Level 2 Analyse Guide", 		mt, {{855,10},{2710,1},8}}
			}
			
			p(9).text = "I have also written books on how to enjoy activities with your fairies. I can give you volume 3 for 50 fairy coins."
			p(9).options = {
				{"Level 3 Manufacturing Guide", mt, {{855,50},{2681,1},9}},
				{"Level 3 Cooking Guide", 		mt, {{855,50},{2691,1},9}},
				{"Level 3 Crafting Guide", 		mt, {{855,50},{2701,1},9}},
				{"Level 3 Analyse Guide", 		mt, {{855,50},{2711,1},9}}
			}
			
			p(10).text = "I have also written books on how to enjoy activities with your fairies. I can give you volume 4 for 150 fairy coins."
			p(10).options = {
				{"Level 4 Manufacturing Guide", mt, {{855,150},{2682,1},10}},
				{"Level 4 Cooking Guide", 		mt, {{855,150},{2692,1},10}},
				{"Level 4 Crafting Guide", 		mt, {{855,150},{2702,1},10}},
				{"Level 4 Analyse Guide", 		mt, {{855,150},{2712,1},10}}
			}
			
			p(11).text = "Oooh, I knew you would like those books. Special release - Volume 5+ now available for Elven signets. Lvl 5 is 5."
			p(11).options = {
				{"Level 5 Manufacturing Guide", mt, {{2588,5},{2683,1},11}},
				{"Level 5 Cooking Guide", 		mt, {{2588,5},{2693,1},11}},
				{"Level 5 Crafting Guide", 		mt, {{2588,5},{2703,1},11}},
				{"Level 5 Analyse Guide", 		mt, {{2588,5},{2713,1},11}}
			}
	
			p(12).text = "Oooh, I knew you would like those books. Special release - Volume 6 is now 25 elven signets."
			p(12).options = {
				{"Level 6 Manufacturing Guide", mt, {{2588,25},{2684,1},12}},
				{"Level 6 Cooking Guide", 		mt, {{2588,25},{2694,1},12}},
				{"Level 6 Crafting Guide", 		mt, {{2588,25},{2704,1},12}},
				{"Level 6 Analyse Guide", 		mt, {{2588,25},{2714,1},12}}
			}
	
			p(13).text = "Oooh, I knew you would like those books. Special release - Volume 7 is now 50 elven signets."
			p(13).options = {
				{"Level 7 Manufacturing Guide", mt, {{2588,50},{2685,1},13}},
				{"Level 7 Cooking Guide", 		mt, {{2588,50},{2695,1},13}},
				{"Level 7 Crafting Guide", 		mt, {{2588,50},{2705,1},13}},
				{"Level 7 Analyse Guide", 		mt, {{2588,50},{2715,1},13}}
			}
	
			p(14).text = "These are my latest and most special books. Special release - Volume 8+ now avaiable for Elven Royal signets. Lvl 8 is 2."
			p(14).options = {
				{"Level 8 Manufacturing Guide", mt, {{2589,2},{2686,1},14}},
				{"Level 8 Cooking Guide", 		mt, {{2589,2},{2696,1},14}},
				{"Level 8 Crafting Guide", 		mt, {{2589,2},{2706,1},14}},
				{"Level 8 Analyse Guide", 		mt, {{2589,2},{2716,1},14}}
			}
			
			p(15).text = "These are my latest and most special books. Special release - Volume 9 is now 5 elven royal signets."
			p(15).options = {
				{"Level 9 Manufacturing Guide", mt, {{2589,5},{2687,1},15}},
				{"Level 9 Cooking Guide", 		mt, {{2589,5},{2697,1},15}},
				{"Level 9 Crafting Guide", 		mt, {{2589,5},{2707,1},15}},
				{"Level 9 Analyse Guide", 		mt, {{2589,5},{2717,1},15}}
			}
			
			p(16).text = "These are my latest and most special books. Final special release - Volume 10 is now 10 elven royal signets."
			p(16).options = {
				{"Level 10 Manufacturing Guide", 	mt, {{2589,10},{2688,1},16}},
				{"Level 10 Cooking Guide", 			mt, {{2589,10},{2698,1},16}},
				{"Level 10 Crafting Guide", 		mt, {{2589,10},{2708,1},16}},
				{"Level 10 Analyse Guide", 			mt, {{2589,10},{2718,1},16}}
			}
			
			p(17).text = "You don't have enough Fairy Items? aww Tingle is sad."
		end
	elseif npc.name == 'Animal Nutritionist' then
		if state == NPC_INIT then
			n().proc = npc_tradeonly_proc
			t().sections = {{
					{0183},{0184},{0185},{0186},
					{0187},{0188},{0189},{0190},
					{0191},{0199},{0262},{1015},
					{0680}
				},{
					{0243},{0244},{0246},{0247},
					{0249},{0250},{0252},{0253},
					{0259},{0260},{1055},{1056},
					{1058},{1059},{1061},{1062},
					{1064},{1065}
				},{
					{0222},{0276},{5721},{5722},
					{0223},{0277},{5723},{5724},
					{0224},{0278},{5725},{5726},
					{0225},{0279},{5727},{5728},
					{0226},{0280},{5729},{5730},
					{0227},{5718},{5719},{5720},
					{0000},{0578}
				},{} }
		end
	end
end

function quests_reset_state(player)
	for i, npc in npcs do
		if table.getn(npc.quests) >= 1 then
			ResetMissionState(player.role, npc.char.role)
		end
	end
end