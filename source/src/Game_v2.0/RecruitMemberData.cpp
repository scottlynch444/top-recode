//------------------------------------------------------------------------
//	2005.5.8	Arcol	create this file
//------------------------------------------------------------------------

#include "stdafx.h"
#include "recruitmemberdata.h"

CRecruitMemberData::CRecruitMemberData() {
	m_dwID = 0;
	m_strName = "";
	m_strJob = "";
	m_dwLv = 0;
}

CRecruitMemberData::~CRecruitMemberData() {
}
