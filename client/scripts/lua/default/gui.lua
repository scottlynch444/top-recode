--*****************************************************************
NORMAL  = 0
HOVER   = 1
DOWN    = 2
DISABLE = 3
ALL     = 4


-----------------------------------------------------------------------
-- 定义宏
-----------------------------------------------------------------------

-- 真假值
TRUE = 1
FALSE = 0

-- 控件显示方式
caLeft =1
caLeftUp =2
caUp = 3
caRightUp = 4
caRight = 5
caRightBottom = 6
caBottom = 7
caLeftBottom = 8
caClient = 9
caCenter  = 10          -- 完全居中
caWidthCenter = 11      -- 水平居中
caHeightCenter = 12     -- 上下居中 

-- 控件类型
-- 20070807 Leo 添加注释 begin
LABEL_TYPE		= 0                     
--label 类型～用来容纳文字，可以用作界面上的不可更改的固定的文字显示用～只支持单行
LABELEX_TYPE		= 1                 
--label的扩展类型～功能同label,可以通过 UI_SetLabelExFont 来设置文字是否带有阴影特效和文字字体格式
BUTTON_TYPE		= 2                 
--button类型～按钮类型，用作界面按钮，装载按钮图时需要用 UI_LoadButtonImage(所属form , 图片源 , 图片源中取的宽 , 图片源中取的高 , 图片源中的X坐标，图片源中的Y坐标 . TRUE/FALSE(是否横向、纵向加载按钮的第2，3，4帧  )
COMBO_TYPE		= 3                 
--下拉(上拉)列表~，聊天频道选择，地图选择等地方又使用 , 程序底层由edit和list 2种控件共同实现，定义之后需要获取该控件的 list 对象来向里面添加选择项
EDIT_TYPE		= 4                         
--编辑框，允许用户输入的控件～可以设置接受输入的最大英文字符数等等属性，游戏中玩家可以输入的地方都是用的这个控件
IMAGE_TYPE		= 5                     
--图片类型～最为广泛使用的控件，form的背景～部分突出的图片如logo等～都是使用的本控件～可以加载一张图片到控件里指定的坐标去，设置控件在form里的坐标就可以了，从图片源里加载的图片会自动放大到 image 控件的大小，因此两者使用同一大小即可～特殊需求除外
LIST_TYPE		= 6
--列表类型～能够容纳多行文字的特殊类型～支持滚动条～游戏中所有的文字列表都是用的该控件～包括聊天显示窗口
PROGRESS_TYPE		= 7
--进度条类型～比较简单的类型～只用设置好坐标，大小以及进度条需要加载哪张图片就好，其余的控制交给程序控制即可
CHECK_TYPE		= 8
--选择框控件～登录界面中的 是否记住该帐号 就是用的本控件，该控件需要加载选中与为选中两种图片状态
CHECK_GROUP_TYPE 	= 9
--单选组控件～系统设置里的选择框组，将多个选择框加载到一个已经定义的单选组控件里去，能够让他们在同一时刻只能处于单选状态
GRID_TYPE		= 10
--表格控件～N X N的表格～通常用来装载游戏中的各种道具，如背包栏，船舱等
PAGE_TYPE		= 11
--分页控件，创建了分页控件之后，可以在分页控件里创建分页，将各种其他元素加载到分页里去，表现位技能栏等效果
FIX_LIST_TYPE		= 12
--没用
CHECK_FIX_LIST_TYPE	= 13
--没用
DRAG_TITLE_TYPE		= 14
--拖动条控件，创建之后可以让该控件随鼠标拖动而移动，聊天窗口的改变大小，滚动条等，都是由这个控件实现
TREE_TYPE		= 15
--tree控件，包含 根 和 枝 两种节点，最典型的应用就是任务栏里的普通任务和任务列表，策划不需要对其做太多操作，定义好并为其设置各种属性即可
IMAGE_FRAME_TYPE	= 16
--没用
UI3D_COMPENT_TYPE	= 17
--D3D控件，用于接受程序以D3D方式绘制的图形（包括文字，2D，3D的各种图形）
MEMO_TYPE		= 18
--多行文本控件，通常策划不会向该控件里添加文字，而是定义好并设置好坐标大小，多少行，每行多少字等属性，然后由程序向里面添加内容
MEMOEX_TYPE		= 19
--多行文本控件的扩展版本，支持特殊内容的添加，如任务奖励界面下方的memoEX ，既有容纳文字，也有容纳奖励 icon等
GOODS_GRID_TYPE		= 20
--货物grid, grid的扩展控件，支持名字和价格，而不是像grid一样只以hint的形式显示信息 ， 使用地点是海盗王里的船舱货物交易面板
FAST_COMMANG_TYPE	= 21
--没用
COMMAND_ONE_TYPE	= 22
--单元格控件，可以在任意地方（仍旧需要属于一个form）定义一个单元格来容纳装备，如角色身上的装备面板～以传统的grid肯定无法制作，因此使用这样的单元格，定义5个格子就好～需要设置大小属性坐标等
IMAGE_FLASH_TYPE	= 23
--没用
SCROLL_TYPE		= 24
--滚动条控件，只在滚动条中使用～策划无需掌握，需要使用时请质询 leo
SKILL_LIST_TYPE		= 25
--技能列表控件～特殊的列表控件，支持icon显示已经其他的内容显示在传统list中
LISTEX_TYPE		= 26
--没用
MENU_TYPE		= 27
--菜单控件，定义各种弹出的菜单，然后向里面添加菜单项～使用地点为人物右键菜单等地方
RICHMEMO_TYPE		= 28
--没用
TITLE_TYPE 		= 29
--没用
RICHEDIT_TYPE 		= 30 
--没用
AMPHI_LIST_TYPE 	= 31
--新增下拉条类型,用于竞技场的复活按钮
UI3D_COMPENT = UI3D_COMPENT_TYPE
WEB_BROWSER_TYPE	= 32

-- 颜色
--COLOR_BLACK = 4278190080
--COLOR_RED = 4294901760
--COLOR_WHITE = 4294967295
--COLOR_PURPLE = 4293990336		-- 紫色
--COLOR_YELLOW = 4294967040           -- 黄色
--COLOR_BLUE = 4278190335 --兰色
--COLOR_GREEN = 4278255360 --绿色
--COLOR_PINK = 4294902015  --粉色

--CHANGED: Luajit supports hex numbers - easier to understand
COLOR_BLACK = 0xFF000000
COLOR_RED = 0xFFFF0000
COLOR_WHITE = 0xFFFFFFFF
COLOR_PURPLE = 0xFFF117C0		-- 紫色
COLOR_YELLOW = 0xFFFFFF00           -- 黄色
COLOR_BLUE = 0xFF0000FF --兰色
COLOR_GREEN = 0xFF00FF00 --绿色
COLOR_PINK = 0xFFFF00FF  --粉色


TREE_TEXT_COLOR = COLOR_WHITE


-- 按钮的四种状态，普通，激活，按下，禁用
NORMAL  = 0
HOVER   = 1
DOWN    = 2
DISABLE = 3

-- 背景图
COMPENT_BACK = 0		-- 控件背景

-- 进度条的两幅画
PROGRESS_PROGRESS = 1 		-- 进度条

-- 选择框的两幅画
UNCHECKED = 0			-- 未选择状态
CHECKED = 1			-- 选择状态

-- 进度条显示风格：
PROGRESS_HORIZONTAL = 0		-- 水平的,从左自右的
PROGRESS_VERTICAL = 1		-- 垂直的，从下直上的


-- 表单样式
-- 0 :普通; 1：全居中，2：x方向居中， 3 Y方向居中， 4：居左； 5：居右；6：居上；7 ：居下 ； 8 左上角，9 右上角，10 左下角 11 右下角
FORM_NONE=0			-- 普通
FORM_ALLCENTER=1		-- 全居中
FORM_XCENTER=2			-- x方向居中
FORM_YCENTER=3			-- Y方向居中
FORM_LEFT=4			-- 居左
FORM_RIGHT=5			-- 居右
FORM_TOP=6			-- 居上
FORM_BOTTOM=7			-- 居下
FORM_LEFTTOP=8			-- 左上角
FORM_RIGHTTOP=9			-- 右上角
FORM_LEFTBOTTOM=10		-- 左下角
FORM_RIGHTBOTTOM=11		-- 右下角


-----------------------------------------------------------------------
-- 定义要调用的函数
-----------------------------------------------------------------------

ALT_KEY = 0
CTRL_KEY = 1
SHIFT_KEY = 2

-- 创建多行列表,失败返回-1
-- 其中style为标题风格
eSimpleTitle = 0		-- 简单标头,即仅一幅图片
eWindowTitle = 1		-- 象windows一样的标头,需要加载每一个标头的图片
eNoTitle     = 2 		-- 没有标题栏

-- 装载按钮的图片
BUTTON_NONE  	= 0
BUTTON_CLOSE	= 1
BUTTON_YES	= 2
BUTTON_NO	= 3
BUTTON_OK	= 4
BUTTON_CANCLE	= 5

-- 得到滚动条中的对象:up按钮,down按钮,scroll图片
SCROLL_UP = 0
SCROLL_DOWN = 1
SCROLL_SCROLL = 2
EXLIST_BUTTON = 3

-- 设置进度条显示风格
PROGRESS_HINT_NUM = 0
PROGRESS_HINT_PERCENT = 1

enumTreeAddImage = 0
enumTreeSubImage = 1
-- 装载树型控件图片,nType为装载是那张图片,图片大小建议16x16,显示宽高itemw,itemh

-- 设置一个Page的点击按钮摆放方式以及单元宽高
PAGE_BUTTON_LEFT_UP=0		-- 依次摆在左上
PAGE_BUTTON_FULL_UP=1		-- 充满上部
PAGE_BUTTON_CUSTOM =2		-- 自定

-- 获得PageItem中的对象
PAGE_ITEM_IMAGE=0
PAGE_ITEM_TITLE=1	

-- 装载PAGE_ITEM_TITLE中图片时的两种状态
PAGE_ITEM_TITLE_NORMAL=0
PAGE_ITEM_TITLE_ACTIVE=1

-- 用于在文本输入框载入表情，其中frame为表情动画帧数
--UI_SetTextParse( 0 , "texture/ui/face/em001.tga", 40, 40 , 0 , 0 ,4  )
--UI_SetTextParse( 1 , "texture/ui/face/em003.tga", 40, 40 , 0 , 0 ,4  )
--UI_SetTextParse( 2 , "texture/ui/face/em001.tga", 40, 40 , 0 , 0 ,4  )
--UI_SetTextParse( 3 , "texture/ui/face/em003.tga", 40, 40 , 0 , 0 ,4  )
--UI_SetTextParse( 4 , "texture/ui/face/em001.tga", 40, 40 , 0 , 0 ,4  )
--UI_SetTextParse( 5 , "texture/ui/face/em003.tga", 40, 40 , 0 , 0 ,4  )
--UI_SetTextParse( 6 , "texture/ui/face/em001.tga", 40, 40 , 0 , 0 ,4  )
--UI_SetTextParse( 7 , "texture/ui/face/em003.tga", 40, 40 , 0 , 0 ,4  )
--UI_SetTextParse( 8 , "texture/ui/face/em001.tga", 40, 40 , 0 , 0 ,4  )
--UI_SetTextParse( 9 , "texture/ui/face/em003.tga", 40, 40 , 0 , 0 ,4  )

UI_ItemBarLoadImage( "texture/ui/system/progress.tga", 64, 16, 0, 0 )


-- ASCII速查表,用于快捷键
HOTKEY_A  = 65
HOTKEY_B  = 66
HOTKEY_C  = 67
HOTKEY_D  = 68
HOTKEY_E  = 69
HOTKEY_F  = 70
HOTKEY_G  = 71
HOTKEY_H  = 72
HOTKEY_I  = 73
HOTKEY_J  = 74
HOTKEY_K  = 75
HOTKEY_L  = 76
HOTKEY_M  = 77
HOTKEY_N  = 78
HOTKEY_O  = 79
HOTKEY_P  = 80
HOTKEY_Q  = 81
HOTKEY_R  = 82
HOTKEY_S  = 83
HOTKEY_T  = 84
HOTKEY_U  = 85
HOTKEY_V  = 86
HOTKEY_W  = 87
HOTKEY_X  = 88
HOTKEY_Y  = 89
HOTKEY_Z  = 90

UI_SetDragSnapToGrid( 4, 4 )

-- 声明所有的表单模板，用于模板切换,对应于程序UITemplete.h
FORM_LOGIN = 0
FORM_MAIN = 1
FORM_SELECT_CHA = 2
FORM_EDITOR = 3
FORM_SWITCH_SCENE = 4
FORM_CREATE_CHA = 5
FORM_SELECT = 6
UI_SetFormTempleteMax( 7 )	-- 设置最大模板数

FORM_DIALOG = FORM_SWITCH_SCENE	-- 对话框模板-4