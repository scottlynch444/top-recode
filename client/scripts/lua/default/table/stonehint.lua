local table_unique = function (t)
	local hash, result = {},{}
	for _,v in ipairs(t) do
		if (not hash[v]) then
			res[table.getn(res)+1] = v
			hash[v] = true
		end
	end
	return result
end

--from lua_functional
local lfunc_foldl = function(func, val, t, iter)
	iter = iter or ipairs
	for k,v in iter(t) do
		val = func(val,v)
	end
	return val
end

local mul = function (x,y) return x * y end --from keyword table in initial.lua in default mod of server files.

--prime number multiplied set table (with 4 colors as primes 2,3,5,7 - for combo color determination)
function Item_Stoneeffect(Color_1, Color_2, Color_3) --not done using table.insert's and calculation because the lua syntax is cumbersome.
	local color_hash = {2,3,5,7,6,10,14,15,21,35,30,42,70,105} --determine item indecomposibility (can't be done in same line)
	return color_hash[lfunc_foldl(mul,1,table_unique({Color_1, Color_2, Color_3}))]-1 --determine item uniqueness
end