skin_name = "default_new"
skin_folder = 'scripts/lua/'..mod..'/skin/' .. skin_name ..'/'

math.randomseed(os.time())

LG("lua", "Hello World!\n")

function _lg(x)
	LG('lua', x)
end

--for i,v in pairs(_G) do
--	_lg(i..'\n')
--end

dofile('scripts/lua/'..mod..'/fov.lua')
dofile('scripts/lua/'..mod..'/res.lua')
dofile('scripts/lua/'..mod..'/mission/mission.lua')
dofile('scripts/lua/'..mod..'/mission/missioninfo.lua')

dofile('scripts/lua/'..mod..'/table/scripts.lua')
dofile('scripts/lua/'..mod..'/table/stonehint.lua')
dofile('scripts/lua/'..mod..'/table/stonehint_raw.lua')

dofile('scripts/lua/'..mod..'/font.lua')
EXTRA_LoadScreen()
dofile('scripts/lua/'..mod..'/gui.lua')
if EXTRA_IsEditor() then dofile('scripts/lua/'..mod..'/editor.lua') end
dofile(skin_folder..'/init.lua')
dofile('scripts/lua/'..mod..'/filter.lua')

function CreateChaScene_Callback()
-- 麓麓陆篓碌脟脗录鲁隆戮掳
	SCENE_CREATECHASCENE_CLU_000001 = GetResString("SCENE_CREATECHASCENE_CLU_000001")
	nCreateScene = SN_CreateScene( enumCreateChaScene, SCENE_CREATECHASCENE_CLU_000001, "", FORM_CREATE_CHA, 300, 200, 100, 100 )	
	SN_SetIsShowMinimap( FALSE )
	SN_SetIsShow3DCursor( FALSE )

	-- 鲁隆戮掳UI
	GP_GotoScene( nCreateScene )	-- GotoScene路脜脭脷脳卯潞贸,脪貌脦陋脡忙录掳脟脨禄禄脛拢掳氓脢卤碌脛卤铆碌楼鲁玫脢录禄炉
end

function LoginScene_Callback()
	-- 麓麓陆篓碌脟脗录鲁隆戮掳
	-- 麓麓陆篓碌脟脗录鲁隆戮掳
	SCENE_LOGINSCENE_CLU_000001 = GetResString("SCENE_LOGINSCENE_CLU_000001")
	nLoginScene = SN_CreateScene( enumLoginScene, SCENE_LOGINSCENE_CLU_000001, "", FORM_LOGIN, 300, 200, 100, 100 )	
	SN_SetIsShowMinimap( FALSE )
	SN_SetIsShow3DCursor( FALSE )

	-- 鲁隆戮掳UI
	UI_ShowForm( frmLOGO, TRUE )

	GP_GotoScene( nLoginScene )	-- GotoScene路脜脭脷脳卯潞贸,脪貌脦陋脡忙录掳脟脨禄禄脛拢掳氓脢卤碌脛卤铆碌楼鲁玫脢录禄炉
end

function MainScene_Callback()
	SCENE_MAINSCENE_CLU_000001 = GetResString("SCENE_MAINSCENE_CLU_000001")
	local nMainScene = SN_CreateScene( enumWorldScene, SCENE_MAINSCENE_CLU_000001, "garner", FORM_MAIN, 300, 400, 400, 600)
	GP_GotoScene( nMainScene )
end

function SelectChaScene_Callback()
	_lg(tostring(1))
	SCENE_SELECTCHASCENE_CLU_000001 = GetResString("SCENE_SELECTCHASCENE_CLU_000001")
	_lg(tostring(2))
	nScene = SN_CreateScene( enumSelectChaScene, SCENE_SELECTCHASCENE_CLU_000001, "", FORM_SELECT, 100, 10, 2000, 300 )	

	_lg(tostring(5))
	GP_GotoScene( nScene )
	_lg(tostring(3))
	SN_SetIsShowMinimap( FALSE )
	_lg(tostring(4))
	SN_SetIsShow3DCursor( FALSE )
	_lg(tostring(6))
end