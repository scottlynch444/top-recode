//
// Generated by Microsoft (R) HLSL Shader Compiler 9.19.949.1104
//
//   fxc /Tvs_1_1 /O3 /Fc../client/shader/dx8/skinmesh8_2_tt2.vsh
//    ../client/shader/dx9_hlsl/skinmesh8_2_tt2.hlsl
//
//
// Parameters:
//
//   float4 Ambient;
//   float4 BV[75];
//   int4 Base;
//   float4 Diffuse;
//   float4 LightDirection;
//   float4x4 Texture2;
//   float4x4 World;
//
//
// Registers:
//
//   Name           Reg   Size
//   -------------- ----- ----
//   Base           c0       1
//   World          c1       4
//   LightDirection c5       1
//   Ambient        c6       1
//   Diffuse        c7       1
//   Texture2       c16      4
//   BV             c21     75
//

    vs.1.1
    //dcl_position v0
    //dcl_blendweight v1
    //dcl_blendindices v2
    //dcl_normal v3
    //dcl_texcoord v7
    mul r0.xy, v1, c0.x
    add r0.x, r0.y, r0.x
    add r0.x, -r0.x, c0.x
    mul r1, v2.xyyw, c0.wwyw
    mov a0.x, r1.y
    dp4 r0.y, v0, c[21+a0.x]
    dp4 r0.z, v0, c[22+a0.x]
    dp4 r0.w, v0, c[23+a0.x]
    mul r0.yzw, r0.x, r0
    mov a0.x, r1.x
    dp4 r2.x, v0, c[21+a0.x]
    dp4 r2.y, v0, c[22+a0.x]
    dp4 r2.z, v0, c[23+a0.x]
    mad r2.xyz, v1.x, r2, r0.yzww
    mov r2.w, c0.x
    dp4 oPos.x, r2, c1
    dp4 oPos.y, r2, c2
    dp4 oPos.z, r2, c3
    dp4 oPos.w, r2, c4
    mov a0.x, r1.y
    dp3 r0.y, v3, c[21+a0.x]
    dp3 r0.z, v3, c[22+a0.x]
    dp3 r0.w, v3, c[23+a0.x]
    mul r0.xyz, r0.x, r0.yzww
    mov a0.x, r1.x
    dp3 r2.x, v3, c[21+a0.x]
    dp3 r2.y, v3, c[22+a0.x]
    dp3 r2.z, v3, c[23+a0.x]
    mov r1.yzw, r1.xzww
    mad r0.xyz, v1.x, r2, r0
    dp3 r0.w, r0, r0
    rsq r0.w, r0.w
    mul r0.xyz, r0, r0.w
    dp3 r1.x, r0, c5
    lit r0, r1
    mov r1, c7
    mad r0, r0.y, r1, c6
    min oD0, r0, c0.x
    mov r0.xyw, v7
    mov r0.z, c0.x
    dp4 oT2.x, r0, c16
    dp4 oT2.y, r0, c17
    dp4 oT2.z, r0, c18
    dp4 oT2.w, r0, c19
    mov oD1, c0.z
    mov oT0, v7
    mov oT1, v7

// approximately 47 instruction slots used
