//HLSL with constrained registers adapted to PKO shaders syntax
//Converting ASM to vs_1_1 accepted by DX8:
//  Change "vs_1_1" to "vs.1.1".
//  Comment all dcl_ instructions (// or ;) - these are already defined in code.
//  Change all c#[?] to c[? + #].

//c# Constants
float4 Base : register(c0);
float4x4 World : register(c1);
float4 Unknown : register(c5);
float4 Ambient : register(c6);
float4 Diffuse : register(c7);
float4 Unknown2 : register(c8);
float4 Unknown3 : register(c9);

struct VertexShaderInput { //v# Input parameters
    float4 Position : POSITION0 : register(v0);
	//float Blendweight : BLENDWEIGHT0 : register(v1);
	//uint Blendindices : BLENDINDICES0 : register(v2);
	float4 Normal : NORMAL0 : register(v3);
	float4 Color : COLOR0 : register(v5);
	float4 TexCoord0 : TEXCOORD0 : register(v7);
	//float4 TexCoord1 : TEXCOORD1 : register(v8);
};
struct VertexShaderOutput { //o# Output parameters
    float4 Position : POSITION0 : register(oP);
	float4 Color : COLOR0 : register(oD0);
	float4 Color1 : COLOR1 : register(oD1);
	float2 TexCoord0 : TEXCOORD0 : register(oT0);
};
VertexShaderOutput main(VertexShaderInput input) {
    VertexShaderOutput output;
    output.Position = mul(input.Position, World);
	output.Color = min(Diffuse+Ambient,Base.x);
	output.Color1 = Base.z;
	float4 test = input.TexCoord0;
	test.z = Base.x;
	output.TexCoord0.x = mul(test, Unknown2);
	output.TexCoord0.y = mul(test, Unknown3);
    return output;
};