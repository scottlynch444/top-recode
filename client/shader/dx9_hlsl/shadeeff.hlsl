//HLSL with constrained registers adapted to PKO shaders syntax
//Converting ASM to vs_1_1 accepted by DX8:
//  Change "vs_1_1" to "vs.1.1".
//  Comment all dcl_ instructions (// or ;) - these are already defined in code.
//  Change all c#[?] to c[? + #].

//BUG: I wasn't able to make this to use the c[0+#] array as needed.
//So I had to change that manually in the .vsh.

//c# General constants
float4 UV[96] : register(c0);

struct VertexShaderInput { //v# Input parameters
    //float4 Position : POSITION0 : register(v0);
	//float Blendweight : BLENDWEIGHT0 : register(v1);
	//uint Blendindices : BLENDINDICES0 : register(v2);
	//float4 Color : COLOR0 : register(v5);
	//float4 TexCoord0 : TEXCOORD0 : register(v7);
	float2 TexCoord1 : TEXCOORD1 : register(v8);
};
struct VertexShaderOutput { //o# Output parameters
    float4 Position : POSITION0 : register(oP);
	float4 Color : COLOR0 : register(oD0);
	float2 TexCoord0 : TEXCOORD0 : register(oT0);
};
VertexShaderOutput main(VertexShaderInput input) {
    VertexShaderOutput output;
	
	float4x4 World = { UV[0].x, UV[1].x, UV[2].x, UV[3].x,
					   UV[0].y, UV[1].y, UV[2].y, UV[3].y,
					   UV[0].z, UV[1].z, UV[2].z, UV[3].z,
					   UV[0].w, UV[1].w, UV[2].w, UV[3].w };
	float4x4 ViewProjection = { UV[4].x, UV[5].x, UV[6].x, UV[7].x,
							    UV[4].y, UV[5].y, UV[6].y, UV[7].y,
							    UV[4].z, UV[5].z, UV[6].z, UV[7].z,
							    UV[4].w, UV[5].w, UV[6].w, UV[7].w };
	float4 Alpha = UV[8];
	
    float4 worldPosition = mul(UV[input.TexCoord1.y], World);
    output.Position = mul(worldPosition, ViewProjection); //2D
	output.Color = Alpha;
	output.TexCoord0 = UV[input.TexCoord1.x];
    return output;
}