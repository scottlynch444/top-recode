//HLSL with constrained registers adapted to PKO shaders syntax
//Converting ASM to vs_1_1 accepted by DX8:
//  Change "vs_1_1" to "vs.1.1".
//  Comment all dcl_ instructions (// or ;) - these are already defined in code.
//  Change all c#[?] to c[? + #].

//c# Constants
uint4 Specular : register(c0);
float4x4 World : register(c1);
uint4 Ambient : register(c6);
uint4 Diffuse : register(c7);

struct VertexShaderInput { //v# Input parameters
    float4 Position : POSITION0 : register(v0);
	//float Blendweight : BLENDWEIGHT0 : register(v1);
	//uint Blendindices : BLENDINDICES0 : register(v2);
	float4 Color : COLOR0 : register(v5);
	float4 TexCoord0 : TEXCOORD0 : register(v7);
	//float4 TexCoord1 : TEXCOORD1 : register(v8);
};
struct VertexShaderOutput { //o# Output parameters
    float4 Position : POSITION0 : register(oP);
	float4 Color : COLOR0 : register(oD0);
	uint4 Color1 : COLOR1 : register(oD1);
	float4 TexCoord0 : TEXCOORD0 : register(oT0);
};
VertexShaderOutput main(VertexShaderInput input) {
    VertexShaderOutput output;
    output.Position = mul(input.Position, World);
	output.Color = clamp((Diffuse*input.Color)+Ambient,0,Specular.x);
	output.Color1 = Specular.zzzz;
	output.TexCoord0 = input.TexCoord0;
    return output;
};