@echo off
@echo Rebuilding all dbs from scratch

@call :execute_sql_scripts "DeleteDBs.sql" "AccountServer.sql" "GameDB.sql" ^
"Guild.sql" "CrystalTrade.sql" "Tradedb.sql" "CreateLogins.sql" "CreateInGameAccount.sql"
@echo ----------------------------------------

pause
@echo on
exit /B

:execute_sql_scripts 
	sqlcmd -X 1 -U sa -d "master" -i %*
	exit /B
